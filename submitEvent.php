<?php
	session_start();
	include 'include/connect.php';
	$pageTitle = 'Submit an Event';
	$eventAdded = isset($_SESSION['event_added']) ? $_SESSION['event_added'] ? true : false : false;

	$action = isset($_GET['action']) ? htmlentities($_GET['action'], ENT_QUOTES) : 'create';
	$id = isset($_GET['id']) ? htmlentities($_GET['id'], ENT_QUOTES) : 0;
	$event;

	if(isset($_POST['btn-submit'])){
		$title = isset($_POST['event-title']) ? htmlentities($_POST['event-title'], ENT_QUOTES) : '';
		$description = isset($_POST['event-description']) ? $_POST['event-description'] : '';
		$dateStart = isset($_POST['input-date-start']) ? $_POST['input-date-start'] : '';
		$dateEnd = isset($_POST['input-date-end']) ? $_POST['input-date-end'] : '';
		$location = isset($_POST['location']) ? $_POST['location'] : '';
		$eventType = 2;
		$imagePath = isset($event['imagePath']) ? $event['imagePath'] : '';
		$isApproved = 0;
		
		// UPLOAD IMAGE TO SERVER FOLDER
		if(isset($_FILES['upload-image'])){
			$file_upload = $_FILES['upload-image'];
			
			if(strlen($file_upload['name']) > 0){
				$target_dir = "uploads/user-generated/event/";
				$basename = basename($file_upload["name"]);
					
				// Check file size
				if ($file_upload["size"] <= 5000000) {
					// Check duplicate file
					$filename = pathinfo($file_upload['name'], PATHINFO_FILENAME);
					$extension = pathinfo($file_upload['name'], PATHINFO_EXTENSION);
					$increment = ''; //start with no suffix

					while(file_exists($target_dir.$filename.$increment.'.'.$extension)) {
						$increment++;
					}
					
					$basename = $filename.$increment.'.'.$extension;
					$image_dir = $target_dir.$basename;
										
					if (move_uploaded_file($file_upload["tmp_name"], $image_dir)) {
						$imagePath = $image_dir;
					} else {
						header("Location: submitEvent.php");
					}
				} else {
					header("Location: submitEvent.php");
				}
			}
		}	
		
		// switch on $action (create, update, unpublish, etc) here
		switch ($action) {
			case 'create':

				$stmt = "
					INSERT INTO 
						`event`( 
							`title`, 
							`description`, 
							`dateStart`, 
							`dateEnd`, 
							`imagePath`, 
							`isApproved`, 
							`eventTypeId`, 
							`location`
						) 
					VALUES (
							?,
							?,
							?,
							?,
							?,
							?,
							?,
							?
						)
				";
				$sql = $mysqli->prepare($stmt);
				$sql->bind_param("sssssiis", $title, $description, $dateStart, $dateEnd, $imagePath, $isApproved, $eventType, $location);
				$sql->execute();
				$sql->close();
				$_SESSION['event_added'] = true;
				break;
		}

		header('Location: submitEvent.php');
	}	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.min.css">
	</head>
<body>
	<?php
		include 'include/navbar.php';
	?>
	<div class="modal fade" tabindex="-1" role="dialog" id='modal-create-success'>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">
						Success!
					</h4>
				</div>
				<div class="modal-body">
					<p>
						Thank you for submitting community event. Your event will be reviewed and added to the website by the admin.
					</p>
					<p> You should receive a confirmation e-mail shortly within the next 48 hours.
						If you do not, please check your Junk or Spam e-mail folder as it may have been mistaken as Junk or Spam e-mail.
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- Page Content -->
    <div class="container">
        <!-- Page Heading -->
        <div class="row">
            <div class="small-box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="heading-text text-center">Create an Event</h2>
                    <hr>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
			<div class="box">
				<div class="col-lg-12">
	                <form action="" class="form-horizontal" name="frm-create-event" id="frm-create-event" method="post" enctype="multipart/form-data">
	                    <div class="form-group">
	                        <label class="control-label col-xs-12 col-sm-4" for="event-title">Event Title</label>
							<div class="col-xs-12 col-sm-6">
								<input type="text" class="form-control" id="event-title" name="event-title" placeholder="Event Title" >
							</div>
	                    </div>
	                    <div class="form-group">
	                        <label class="control-label col-xs-12 col-sm-4" for="event-description">Event Description</label>
							<div class="col-xs-12 col-sm-6">
								<textarea class="form-control" id="event-description" name="event-description" rows="3" placeholder="Event Description"></textarea>
							</div>
	                    </div>
						
	                    <div class="form-group">
	                       <label for='input-date-start' class='control-label col-xs-12 col-sm-4'>Start Date </label>
						   <div class='col-xs-12 col-sm-6'>
							<div class='input-group date' id='picker-date-start' name='picker-date-start'>
								<input type='text' class='form-control' name='input-date-start' id='input-date-start' placeholder='Start Date' />
								<span class='input-group-addon'>
									<span class='glyphicon glyphicon-calendar'></span>
								</span>
							</div>
						   </div>
	                    </div>
						
	                     <div class="form-group">
	                       <label for='input-date-start' class='control-label col-xs-12 col-sm-4'>End Date </label>
						   <div class='col-xs-12 col-sm-6'>
							<div class='input-group date' id='picker-date-end'>
								<input type='text' class='form-control' name='input-date-end' id='input-date-end' placeholder='End Date' />
								<span class='input-group-addon'>
									<span class='glyphicon glyphicon-calendar'></span>
								</span>
							</div>
						   </div>
	                    </div>
							
						<div class="form-group">
	                        <label class="control-label col-xs-12 col-sm-4" for="location">Location</label>
							<div class="col-xs-12 col-sm-6">
								<input type="text" class="form-control" id="location" name="location" placeholder="Location">
							</div>
	                    </div>
	                    <div class="form-group">
	                        <label class="control-label col-xs-12 col-sm-4" for="upload-image">Upload Image</label>
	                        <div class="col-xs-12 col-sm-6">
								<input name='upload-image' type="file" placeholder="Browse File." class="form-control-file">
							</div>
	                    </div>
						<div class="form-group"> 
							<div class="col-sm-offset-4 col-sm-4">
								 <button type="submit" name="btn-submit" class="btn btn-default">Submit</button>
							</div>
						</div>
	                </form>
	            </div>
			</div>
        </div>
	</div>
    <!-- /.container -->
	<?php
		include 'include/js.php';
	?>

	<script src="js/jquery.validate.min.js"></script>
	<script src="js/additional-methods.min.js"></script>
	<script src="js/phoneUS.js"></script>
	<script src="js/moment.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<script src="js/createMyKittyAndCommunityEvent.js"></script>
	<?php
		if($eventAdded){
	?>
		<script>
			$('#modal-create-success').modal('toggle')
		</script>
	<?php
			$_SESSION['event_added'] = false;
		}

	?>
</body>
</html>
