<?php
	session_start();

	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			include 'include/connect.php';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
		include 'include/specialtyItemAddedSuccesMes.php';
	?>
		<div class="container">
		<br/>
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<!-- insert content here -->
						<h1 class='text-center'>
							Create Specialty Item
						</h1><br/>
						<form class='form-horizontal' method="post" name="frm-speciality-item" id="frm-speciality-item" enctype="multipart/form-data">
							<div class='form-group'>
								<label class='control-label col-xs-6'>
									Category
								</label>
								<div class='col-xs-6'>
									<select class='form-control' name="item-category">
										<option value="default"></option>
										<option value="Drinks">Drinks</option>
										<option value="Snacks">Snacks</option>
										<option value="Food">Food</option>
									</select>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-xs-6'>
									Name
								</label>
								<div class='col-xs-6'>
									<input type='text' class='form-control' name="item-name" id="item-name" />
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-xs-6'>
									Description
								</label>
								<div class='col-xs-6'>
									<textarea class='form-control' rows='3' name="item-description" id="item-description"></textarea>
								</div>
							</div>
							<div class='form-group'>
								<label for='' class='control-label col-xs-6'>
									Image
								</label>
								<div class='col-xs-6'>
									<input type="file" id="item-image" name="item-image" id="item-image">
									<p class="help-block">
										.png or .jpg only.
									</p>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-xs-6'>
								</label>
								<div class='col-xs-6'>
									<input type='submit' class='btn btn-success' value='Submit' name="add-specialty-item" id="add-specialty-item" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

<?php include 'include/js.php'; ?>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/addSpecialityItem.js"></script>
	<?php

		if (isset($_POST['add-specialty-item'])) {
			$name = isset($_POST['item-name']) ? $mysqli->real_escape_string($_POST['item-name']) : '';
			$description = isset($_POST['item-description']) ? $mysqli->real_escape_string($_POST['item-description']) : '';
			$catImage = isset($_FILES['item-image']) ? $_FILES['item-image'] : '';
			$category = isset($_POST['item-category']) ? $_POST['item-category'] : '';

			$target_dir = "uploads/specialties/";
			$newImgPath = $target_dir . basename($catImage['name']);

			if ($catImage["size"] <=500000) {
				if (move_uploaded_file($catImage["tmp_name"], $newImgPath)) {
				}
			}
			if ($stmt = $mysqli->prepare("INSERT INTO specialtyitems (name, category, description, imgPath) VALUES (?, ?, ?, ?)")) {

				$stmt->bind_param("ssss", $name, $category, $description, $newImgPath);

				if ($stmt->execute()) {
				?>
				<script>
					$('#modal-specialty-added').modal('toggle')
				</script>
				<?php
					$stmt->close();
				}
			}

		}
	?>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>
