<?php
	session_start();
	include 'include/connect.php';
	
	if(isset($_SESSION['logged_in'])){
		$action = isset($_GET['action']) ? htmlentities($_GET['action'], ENT_QUOTES) : '';
		
		switch($action){
			case 'logout':
				unset($_SESSION['logged_in']);
				unset($_SESSION['user_type']);
				header('Location: login.php');
				
				break;
			default:
				header('Location: cms.php');
		}
	}
	
	if(isset($_POST['btn-submit'])){
		$username = isset($_POST['input-username']) ? htmlentities($_POST['input-username'], ENT_QUOTES) : '';
		$password = isset($_POST['input-password']) ? htmlentities($_POST['input-password'], ENT_QUOTES) : '';
		
		$stmt = "
			SELECT 
				`username`, 
				`hash`, 
				`userTypeId` 
			FROM 
				`user` 
			WHERE 
				username = ?
		";
		
		if($sql = $mysqli->prepare($stmt)){
			$sql->bind_param("s", $username);
			$sql->execute();
			$res = $sql->get_result();
			
			if($res->num_rows == 1){
				$row = $res->fetch_assoc();
				$hash = $row['hash'];
				
				if(password_verify($password, $hash)){
					$_SESSION['logged_in'] = true;
					$_SESSION['user_type'] = $row['userTypeId'];
					header('Location: cms.php');
				} else {
					$_SESSION['message'] = 'Incorrect username/password.';
					header('Location: login.php');
				}
			} else {
				$_SESSION['message'] = 'Incorrect username/password.';
				header('Location: login.php');
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<h1 class='text-center'>
							Login
						</h1>
						<p class='text-center'>
						<?php
							isset($_SESSION['message']) ? print $_SESSION['message']: '';
						?>
						</p>
						<form action='' method='post' name='frm-login' id='frm-login' class='form-horizontal'>
							<div class='form-group'>
								<label for='input-username' class='control-label col-xs-6'>
									Username
								</label>
								<div class='col-xs-6'>
									<input type='text' class='form-control' name='input-username' id='input-username'/>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-password' class='control-label col-xs-6'>
									Password
								</label>
								<div class='col-xs-6'>
									<input type='password' class='form-control' name='input-password' id='input-password'/>
								</div>
							</div>
							<div class='form-group'>
								<div class='col-xs-6 col-xs-push-6'>
									<input type='submit' class='btn btn-lg btn-success' name='btn-submit' id='btn-submit' value='Login'/>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	<?php
		include 'include/js.php';
	?>
	</body>
</html>