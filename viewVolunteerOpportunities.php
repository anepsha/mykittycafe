<?php 
	session_start();
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			include 'include/connect.php';
			
			$pageTitle = 'Volunteer Opportunities';
			$action = isset($_GET['action']) ? htmlentities($_GET['action'], ENT_QUOTES) : '';
			$id = isset($_GET['id']) ? htmlentities($_GET['id'], ENT_QUOTES) : 0;
			
			switch($action){
				case 'publish':
					$publish = 1;
					$stmt = "
						UPDATE 
							`volunteeropportunity` 
						SET 
							`publish`=?
						WHERE 
							id = ?
					";
					$sql = $mysqli->prepare($stmt);
					$sql->bind_param("ii", $publish, $id);
					$sql->execute();
					$sql->close();
					
					header('Location: viewVolunteerOpportunities.php');
					
					break;
				case 'unpublish':
					$publish = 0;
					$stmt = "
						UPDATE 
							`volunteeropportunity` 
						SET 
							`publish`=?
						WHERE 
							id = ?
					";
					$sql = $mysqli->prepare($stmt);
					$sql->bind_param("ii", $publish, $id);
					$sql->execute();
					$sql->close();
					
					header('Location: viewVolunteerOpportunities.php');
					
					break;
			}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>
		<?php echo $pageTitle; ?> - My Kitty Cafe
		</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<!-- enter page content here -->
						<h1 class='text-center'>
						<?php
							echo $pageTitle;
						?>
						</h1>
						<a href='addVolunteerOpportunity.php' class='btn btn-primary pull-right'>Create Volunteer Opportunity</a><br/>
						<br/>
						<table class='table table-striped'>
							<tr>
								<th>
									Title
								</th>
								<th>
									Date Start
								</th>
								<th>
									Date End
								</th>
								<th>
									
								</th>
							</tr>
							<?php
								$stmt = "
									SELECT 
										`id`, 
										`title`, 
										`dateStart`, 
										`dateEnd`,
										`publish`
									FROM 
										`volunteeropportunity`
								";
								$sql = $mysqli->prepare($stmt);
								$sql->execute();
								$res = $sql->get_result();
								
								while($row = $res->fetch_assoc()){
									$dateStart = strtotime($row['dateStart']);
									$dateStartDisplay = date('Y/m/d', $dateStart);
									$dateEnd = strtotime($row['dateEnd']);
									$dateEndDisplay = date('Y/m/d', $dateEnd);
							?>
							<tr>
								<td>
								<?php
									echo $row['title'];
								?>
								</td>
								<td>
								<?php
									echo $dateStartDisplay;
								?>
								</td>
								<td>
								<?php
									echo $dateEndDisplay;
								?>
								</td>
								<td>
								<?php
									if($row['publish']){
								?>
									<a href='viewVolunteerOpportunities.php?action=unpublish&id=<?php echo $row['id']; ?>' class='btn btn-default'>Unpublish</a>
								<?php
									} else {
								?>
									<a href='viewVolunteerOpportunities.php?action=publish&id=<?php echo $row['id']; ?>' class='btn btn-default'>Publish</a>
								<?php
									}
								?>
									<a href='addVolunteerOpportunity.php?action=update&id=<?php echo $row['id']; ?>' class='btn btn-default'>Update</a>
								</td>
							</tr>
							<?php
								}
								
								$sql->close();
							?>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	<?php
		include 'include/js.php';
	?>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>