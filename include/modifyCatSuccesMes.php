<div class="modal fade" id="modal-cat-updated" class="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Success!</h4>
      </div>
      <div class="modal-body">
        <p>Cat record updated successfully!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-cat-added" data-dismiss="modal" onclick="window.location.href='catsListing.php'">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->