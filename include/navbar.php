<div class="brand-heading">
	My Kitty Cafe
</div>
<div class="havbar-address">
	117 Wyndham Street North | Guelph, ON N1H 4E9 | 226-979-5775 | mykittycafe@yahoo.ca
</div>
<!-- Navigation -->
<nav class="navbar navbar-default" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">MyKittyCafe</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li>
					<a href="index.php">Home</a>
				</li>
				<li>
					<a href="cats.php">Meows</a>
				</li>
				<li>
					<a href="menu.php">Menu</a>
				</li>
				<li>
					<a href="myKittyCafeEvent.php">MyKittyCafeEvent</a>
				</li>
				<li>
					<a href="communityEvent.php">Community Events</a>
				</li>
				<li>
					<a href="specials.php">Specials</a>
				</li>
				<li>
					<a href="volunteer.php">Volunteer</a>
				</li>
				<?php
					if(isset($_SESSION['logged_in'])){
						if($_SESSION['logged_in']){
				?>
				<li>
					<a href="cms.php">CMS</a>
				</li>
				<li>
					<a href="login.php?action=logout">Logout</a>
				</li>
				<?php
						}
					}
				?>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>