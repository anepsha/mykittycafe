<div class="modal fade" id="addNewCatModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Add New Cat
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

              <form action="" method="post" name="frm-add-new-cat" id="frm-add-new-cat" enctype="multipart/form-data">

                <fieldset class="form-group">
                  <label for="cat-name">Cat Name</label>
                  <input type="text" class="form-control" id="cat-name" placeholder="Cat Name" name="cat-name">
                </fieldset>

                <fieldset class="form-group">
                  <label for="cat-description">Cat Description</label>
                  <textarea class="form-control" id="cat-description" name="cat-description" rows="10" placeholder="Cat Description"></textarea>
                </fieldset>

                <fieldset class="form-group">
                  <label for="cat-image-upload">Cat Image</label>
                  <input type="file" name="cat-image-upload" id="cat-image-upload">
                </fieldset>

                <fieldset class="form-group">
                  <img id="image-preview" class="image-responsive" style="width: 270px">
                </fieldset>

            </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                                Close
                    </button>
                    <input type="submit" id="add-new-cat" name="add-new-cat" value="Add New Cat" class="btn btn-success">
                </div>
              </form>
        </div>
    </div>
</div>
