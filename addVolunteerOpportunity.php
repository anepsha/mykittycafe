<?php
	session_start();
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			include 'include/connect.php';
			
			$pageTitle = 'Add Volunteer Opportunity';
			$added = isset($_SESSION['volunteer_opportunity_added']) ? $_SESSION['volunteer_opportunity_added'] ? true : false : false;
			$updated = isset($_SESSION['volunteer_opportunity_updated']) ? $_SESSION['volunteer_opportunity_updated'] ? true : false : false;
			$action = isset($_GET['action']) ? htmlentities($_GET['action'], ENT_QUOTES) : '';
			$id = isset($_GET['id']) ? htmlentities($_GET['id'], ENT_QUOTES) : '';
			
			if(isset($_POST['btn-submit'])){
				$volunteerTitle = isset($_POST['input-title']) ? htmlentities($_POST['input-title'], ENT_QUOTES) : '';
				$volunteerDescription = isset($_POST['input-description']) ? htmlentities($_POST['input-description'], ENT_QUOTES) : '';
				$volunteerDateStart = isset($_POST['input-date-start']) ? htmlentities($_POST['input-date-start'], ENT_QUOTES) : '';
				$volunteerDateEnd = isset($_POST['input-date-end']) ? htmlentities($_POST['input-date-end'], ENT_QUOTES) : '';
				$volunteerHoursPerWeek = isset($_POST['input-hours-per-week']) ? htmlentities($_POST['input-hours-per-week'], ENT_QUOTES) : '';
				$volunteerContactName = isset($_POST['input-contact-name']) ? htmlentities($_POST['input-contact-name'], ENT_QUOTES) : '';
				$volunteerContactEmail = isset($_POST['input-contact-email']) ? htmlentities($_POST['input-contact-email'], ENT_QUOTES) : '';
				$volunteerContactPhone = isset($_POST['input-contact-phone']) ? htmlentities($_POST['input-contact-phone'], ENT_QUOTES) : '';
				$volunteerLocation = isset($_POST['input-location']) ? htmlentities($_POST['input-location'], ENT_QUOTES) : '';
				$volunteerRequiredResume = isset($_POST['checkbox-additional-documents-required-resume']) ? 1 : 0;
				$volunteerRequiredCoverLetter = isset($_POST['checkbox-additional-documents-required-cv']) ? 1 : 0;
				
				switch($action){
					case 'update':
						$stmt = "
							UPDATE 
								`volunteeropportunity` 
							SET 
								`title`=?,
								`description`=?,
								`dateStart`=?,
								`dateEnd`=?,
								`hoursPerWeek`=?,
								`contactName`=?,
								`contactEmail`=?,
								`contactPhone`=?,
								`location`=?,
								`requireResume`=?,
								`requireCoverLetter`=?
							WHERE
								id = ?
						";
						$sql = $mysqli->prepare($stmt);
						$sql->bind_param("ssssissssiii", $volunteerTitle, $volunteerDescription, $volunteerDateStart, $volunteerDateEnd, $volunteerHoursPerWeek, $volunteerContactName, $volunteerContactEmail, $volunteerContactPhone, $volunteerLocation, $volunteerRequiredResume, $volunteerRequiredCoverLetter, $id);
						
						if($sql->execute()){
							$_SESSION['volunteer_opportunity_updated'] = true;
						} else {
							$_SESSION['volunteer_opportunity_updated'] = false;
						}
						
						$sql->close();
						header('Location: addVolunteerOpportunity.php?action=update&id='.$id);
						break;
					default:
						$stmt = "
							INSERT INTO volunteeropportunity(
								title, 
								description,
								dateStart,
								dateEnd,
								hoursPerWeek, 
								contactName, 
								contactEmail, 
								contactPhone, 
								location,
								requireResume,
								requireCoverLetter
							) 
							VALUES (
								'".$volunteerTitle."',
								'".$volunteerDescription."',
								'".$volunteerDateStart."',
								'".$volunteerDateEnd."',
								".$volunteerHoursPerWeek.",
								'".$volunteerContactName."',
								'".$volunteerContactEmail."',
								'".$volunteerContactPhone."',
								'".$volunteerLocation."',
								".$volunteerRequiredResume.",
								".$volunteerRequiredCoverLetter."
							)
						";
						$sql = $mysqli->prepare($stmt);
						$sql->bind_param("ssssissssii", $volunteerTitle, $volunteerDescription, $volunteerDateStart, $volunteerDateEnd, $volunteerHoursPerWeek, $volunteerContactName, $volunteerContactEmail, $volunteerContactPhone, $volunteerLocation, $volunteerRequiredResume, $volunteerRequiredCoverLetter);
						
						if($sql->execute()){
							$_SESSION['volunteer_opportunity_added'] = true;
						} else {
							$_SESSION['volunteer_opportunity_added'] = false;
						}
						
						$sql->close();
						header('Location: addVolunteerOpportunity.php');
				}
			}
			
			$volunteerOpportunity;
				
			switch($action){
				case 'update':
					$stmt = "
						SELECT 
							`title`, 
							`description`, 
							`dateStart`, 
							`dateEnd`, 
							`hoursPerWeek`, 
							`contactName`, 
							`contactEmail`, 
							`contactPhone`, 
							`location`, 
							`requireResume`, 
							`requireCoverLetter`
						FROM 
							`volunteeropportunity` 
						WHERE
							id = ?
					";
					$sql = $mysqli->prepare($stmt);
					$sql->bind_param("i", $id);
					$sql->execute();
					$res = $sql->get_result();
					
					if($res->num_rows == 1){
						$volunteerOpportunity = $res->fetch_assoc();
					}
					
					$sql->close();
					break;
			}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>
		<?php
			echo $pageTitle.' - My Kitty Cafe';
		?>
		</title>
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
		<!-- https://eonasdan.github.io/bootstrap-datetimepicker/ -->
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	</head>
	<body>
		<div class="modal fade" tabindex="-1" role="dialog" id='modal-added'>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							Success!
						</h4>
					</div>
					<div class="modal-body">
						<p>
							Volunteer opportunity successfully added!
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<div class="modal fade" tabindex="-1" role="dialog" id='modal-updated'>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							Success!
						</h4>
					</div>
					<div class="modal-body">
						<p>
							Volunteer opportunity successfully updated!
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="box">
					<div class="row">
						<div class="col-xs-12">
							<h1 class='text-center' style='margin-top: 0;'>
								Add Volunteer Opportunity
							</h1>
							<form action='' method='post' class='form-horizontal' name='frm-add-volunteer' id='frm-add-volunteer'>
								<div class='form-group'>
									<label for='input-title' class='control-label col-xs-6'>
										Title
									</label>
									<div class='col-xs-6'>
										<input type='text' class='form-control' name='input-title' id='input-title' value='<?php isset($volunteerOpportunity['title']) ? print $volunteerOpportunity['title'] : ''; ?>'/>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-description' class='control-label col-xs-6'>
										Description
									</label>
									<div class='col-xs-6'>
										<textarea class='form-control' name='input-description' id='input-description' rows='3'><?php isset($volunteerOpportunity['description']) ? print $volunteerOpportunity['description'] : ''; ?></textarea>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-date-start' class='control-label col-xs-6'>
										Date Start
									</label>
									<div class='col-xs-6'>
										<div class='input-group date' id='picker-date-start'>
											<input type='text' class='form-control' name='input-date-start' id='input-date-start' value='<?php isset($volunteerOpportunity['dateStart']) ? print $volunteerOpportunity['dateStart'] : ''; ?>'/>
											<span class='input-group-addon'>
											<span class='glyphicon glyphicon-calendar'></span>
											</span>
										</div>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-date-end' class='control-label col-xs-6'>
										Date End
									</label>
									<div class='col-xs-6'>
										<div class='input-group date' id='picker-date-end'>
											<input type='text' class='form-control' name='input-date-end' id='input-date-end' value='<?php isset($volunteerOpportunity['dateEnd']) ? print $volunteerOpportunity['dateEnd'] : ''; ?>'/>
											<span class='input-group-addon'>
												<span class='glyphicon glyphicon-calendar'></span>
											</span>
										</div>

									</div>
								</div>
								<div class='form-group'>
									<label for='input-hours-per-week' class='control-label col-xs-6'>
										Hours Per Week
									</label>
									<div class='col-xs-6'>
										<input type='text' class='form-control' name='input-hours-per-week' id='input-hours-per-week'  value='<?php isset($volunteerOpportunity['hoursPerWeek']) ? print $volunteerOpportunity['hoursPerWeek'] : ''; ?>'/>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-contact-name' class='control-label col-xs-6'>
										Contact Name
									</label>
									<div class='col-xs-6'>
										<input type='text' class='form-control' name='input-contact-name' id='input-contact-name'  value='<?php isset($volunteerOpportunity['contactName']) ? print $volunteerOpportunity['contactName'] : ''; ?>'/>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-contact-email' class='control-label col-xs-6'>
										Contact Email
									</label>
									<div class='col-xs-6'>
										<input type='text' class='form-control' name='input-contact-email' id='input-contact-email' value='<?php isset($volunteerOpportunity['contactEmail']) ? print $volunteerOpportunity['contactEmail'] : ''; ?>'/>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-contact-phone' class='control-label col-xs-6'>
										Contact Phone
									</label>
									<div class='col-xs-6'>
										<input type='text' class='form-control' name='input-contact-phone' id='input-contact-phone' value='<?php isset($volunteerOpportunity['contactPhone']) ? print $volunteerOpportunity['contactPhone'] : ''; ?>'/>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-location' class='control-label col-xs-6'>
										Location
									</label>
									<div class='col-xs-6'>
										<input type='text' class='form-control' name='input-location' id='input-location' value='<?php isset($volunteerOpportunity['location']) ? print $volunteerOpportunity['location'] : ''; ?>'/>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-location' class='control-label col-xs-6'>
										Additional Documents Required
									</label>
									<div class='col-xs-6'>
										<div class='checkbox'>
											<label>
												<input type='checkbox' value='resume' name='checkbox-additional-documents-required-resume' id='input-additional-documents-required-resume' <?php isset($volunteerOpportunity['requireResume']) ? $volunteerOpportunity['requireResume'] ? print 'checked' : '' : ''?>>
												Resume
											</label>
										</div>
										<div class='checkbox'>
											<label>
												<input type='checkbox' value='cv' name='checkbox-additional-documents-required-cv' id='input-additional-documents-required-cv' <?php isset($volunteerOpportunity['requireCoverLetter']) ? $volunteerOpportunity['requireCoverLetter'] ? print 'checked' : '' : ''?>>
												Cover Letter
											</label>
										</div>
									</div>
								</div>
								<div class='form-group'>
									<div class='col-xs-6 col-xs-push-6'>
										<button type='submit' class='btn btn-success' name='btn-submit' id='btn-submit'>
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Submit
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
		include 'include/js.php';
	?>
		<script src="js/jquery.validate.min.js"></script>
		<script src="js/additional-methods.min.js"></script>
		<script src="js/phoneUS.js"></script>
		<script src="js/moment.js"></script>
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<script src="js/addVolunteerOpportunity.js"></script>
		<?php
			if($added){
		?>
		<script>
			$('#modal-added').modal('toggle')
		</script>
		<?php
				$_SESSION['volunteer_opportunity_added'] = false;
			}
			
			if($updated){
		?>
		<script>
			$('#modal-updated').modal('toggle')
		</script>
		<?php
				$_SESSION['volunteer_opportunity_updated'] = false;
			}
		?>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>