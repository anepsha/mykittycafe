$.validator.addMethod(
	"validateDate",
	function(value, element) {
		return value.match(/[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]/);
	}
);
$().ready(function(){
	$('#frm-create-event').validate({
		rules:{
			'event-title':{
				required: true
			},
			'event-description':{
				required: true
			},
			'input-date-start':{
				required: true,
				validateDate: true
			},
			'input-date-end':{
				required: true,
				validateDate: true
			},
			'location':{
				required: true,
			}
		}
	});
	
	$('#picker-date-start').datetimepicker({
		'format': 'YYYY-MM-DD HH:mm'
		/* 'format': 'YYYY-MM-DD HH:mm:ss',
		'sideBySide': true */
	});
	
	$('#picker-date-end').datetimepicker({
		'format': 'YYYY-MM-DD HH:mm'
		/* 'format': 'YYYY-MM-DD HH:mm:ss',
		'sideBySide': true */
	});
	
	$('#frm-create-event').on('submit', function(){
		if($('#frm-create-event').valid() == false){
			return false;
		}
	});
});