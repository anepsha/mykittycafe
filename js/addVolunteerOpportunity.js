$().ready(function(){
	$('#frm-add-volunteer').validate({
		rules:{
			'input-title':{
				required: true
			},
			'input-description':{
				required: true
			},
			'input-date-start':{
				required: true,
				dateISO: true
			},
			'input-date-end':{
				required: true,
				dateISO: true
			},
			'input-hours-per-week':{
				required: true,
				digits: true
			},
			'input-contact-name':{
				required: true
			},
			'input-contact-email':{
				required: true,
				email: true
			},
			'input-contact-phone':{
				required: true,
				phoneUS: true
			},
			'input-location':{
				required: true
			}
		}
	});
	
	$('#picker-date-start').datetimepicker({
		'format': 'YYYY-MM-DD'
	});
	
	$('#picker-date-end').datetimepicker({
		'format': 'YYYY-MM-DD'
	});
	
	$('#frm-add-volunteer').on('submit', function(){
		if($('#frm-add-volunteer').valid() == false){
			return false;
		}
	});
});