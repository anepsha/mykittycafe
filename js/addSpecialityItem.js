 $.validator.addMethod("valueNotEquals", function(value, element, arg){
  return arg != value;
 }, "Value must not equal arg.");

$().ready(function(){
	$('#frm-speciality-item').validate({
		rules:{
			'item-category' : {
				valueNotEquals: "default"
			},
			'item-name' : {
				required: true
			},
			'item-description' : {
				required: true
			},
			'item-image' : {
				required: true
			}
		},
		messages: {
			'item-category' : {
				valueNotEquals: "Select a category"
			}
		}
	});
});