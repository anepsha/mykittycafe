$().ready(function(){
	$('#frm-add-new-cat').validate({
		rules:{
			'cat-name' : {
				required: true
			},
			'cat-description' : {
				required: true
			},
			'cat-image-upload' : {
				required: true
			}
		}
	});
});