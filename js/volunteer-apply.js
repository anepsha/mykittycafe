$().ready(function(){
	$('#frm-volunteer-apply').validate({
		rules:{
			'input-first-name':{
				required: true
			},
			'input-last-name':{
				required: true
			},
			'input-email':{
				required: true,
				email: true
			},
			'input-phone':{
				required: true,
				phoneUS: true
			}
		}
	});
	
	$('#frm-volunteer-apply').on('submit', function(){
		if($('#frm-volunteer-apply').valid()){
			if(grecaptcha.getResponse() == ""){
				return false;
			}
		} else {
			return false;
		}
	});
});