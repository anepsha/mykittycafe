$().ready(function(){
	$('#frm-email-subscription').validate({
		rules:{
			'email-subscription':{
				required: true,
				email: true
			}
		},
		message:{
			'email-subscription':{
				required: "This field is required",
				email: "Please enter a valid email address"
			}
		}
	});
	$('#frm-email-subscription').on('submit',function(){
		if($('#frm-email-subscription').valid()==false){
			return false;
		}
	});
});