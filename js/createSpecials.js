$().ready(function(){
	$('#frm-create-special').validate({
		rules:{
			'input-title':{
				required: true
			},
			'input-description':{
				required: true
			},
			'input-date-start':{
				required: false,
				dateISO: true
			},
			'input-date-end':{
				required: false,
				dateISO: true
			},
			'input-image':{
				accept: "image/jpeg, image/gif, image/png",
				filesize: 5000000
			}
		}
	});
	
	$('#section-discount').hide();
	$('#picker-date-start').datetimepicker({
		'format': 'YYYY-MM-DD'
	});
	$('#picker-date-end').datetimepicker({
		'format': 'YYYY-MM-DD'
	});
	$("input[name='input-type']").on('change', function(){
		switch($(this).val()){
			case '1':
				if(jQuery.isEmptyObject($('#input-discount').rules()) == false){
					$('#input-discount').rules('remove');
				}
				$('#section-discount').hide();
				break;
			case '2':
				$('#section-discount').show();
				$('#input-discount').rules('add',{
					required: true
				});
				break;
		}
	});
	
	$('#frm-create-special').on('submit', function(){
		if($('#frm-create-special').valid() == false){
			return false;
		}
	});
});