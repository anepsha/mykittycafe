<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cats</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/1-col-portfolio.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/styles.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div class="brand-heading">My Kitty Cafe</div>
    <div class="havbar-address">117 Wyndham Street North | Guelph, ON N1H 4E9 | 226-979-5775 | mykittycafe@yahoo.ca</div>
    <!-- Navigation -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">MyKittyCafe</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="cats.php">Meows</a>
                    </li>
                    <li>
                        <a href="myKittyCafeEvent.php">MyKittyCafeEvent</a>
                    </li>                    
                    <li>
                        <a href="communityEvent.php">Community Events</a>
                    </li>
                    <li>
                        <a href="sales.php">Sales</a>
                    </li>
                    <li>
                        <a href="vol.php">Volunteer</a>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">CMS <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="menuCMS.php">UploadMenuAdmin</a></li>
                            <li><a href="kittyEventCMS.php">MyKittyCafeEventAdmin</a></li>
                            <li><a href="communityEventCMS.php">CommunityEventAdmin</a></li>
                            <li><a href="salesPromotions.php">Sales/Promotions</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="small-box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="heading-text text-center">Adoption Form</h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box">
                <form>
                    <fieldset class="form-group">
                        <label for="fullName">Full Name</label>
                        <input type="text" class="form-control" id="fullName" placeholder="Full Name" required="true">
                    </fieldset>
                  
                    <fieldset class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                        <small class="text-muted">We'll never share your email with anyone else.</small>
                    </fieldset>
                  
                    <fieldset class="form-group">
                        <label for="phoneNumber">Phone Number</label>
                        <input type="password" class="form-control" id="phoneNumber" placeholder="Phone Number" required="true">
                    </fieldset>

                    <fieldset class="form-group">
                        <label for="exampleTextarea">Message</label>
                        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                    </fieldset>
                    <a href="formSubmited.php" class="btn btn-default" role="button">Submit</a>
                </form>
            </div>
        </div>

        <!-- /.row -->
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; MyKittyCafe 2016</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
