<?php
	session_start();
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			include 'include/connect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
	<?php
		include 'include/meta.php';
	?>
        <title>My Kitty Cafe</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/1-col-portfolio.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
    </head>
    <body>
    <?php
        include 'include/menuAddedSuccesMes.php';
        include 'include/navbar.php'
    ?>
        <div class="container"> 
            <br/>
            <div class="row">
                <div class="box">
                    <div class="col-lg-12">
                    <h1 class='text-center'>
                        Update Menu
                    </h1><br/>
                    <!-- insert content here -->
                        <form class='form-horizontal' method="post" enctype="multipart/form-data">
                            <div class='form-group'>
                                <label for='image-preview' class='control-label col-xs-6'>
                                    Menu Image
                                </label>
                                <div class='col-xs-6'>
                                    <input type="file" id="menu-image-input" name="menu-image-input">
                                    <p class="help-block">
                                        .png or .jpg only.
                                    </p>
                                </div>
                                <label for="image-preview" class='control-label col-xs-6'>Image Preview</label>
                                <div class="col-xs-6">
                                    <img id="image-preview" class="image-responsive" style="width: 350px">
                                </div>
                            </div>
                            <div class='form-group'>
                                <div class='col-xs-6 col-xs-push-6'>
                                    <input type='submit' class='btn btn-success' name="submit-menu-image" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<?php include 'include/js.php';?>
    <script>
        $("#menu-image-input").on('change', function () {
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();

            if (extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof (FileReader) != "undefined") {

                    var image_holder = $("#image-preview");
                    image_holder.empty();

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("<img />", {
                            "src": e.target.result,
                                "class": "image-responsive",
                                "style": "width: 270px"
                        }).appendTo(image_holder);

                    }
                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Pls select only images");
            }
        });
    </script>
<?php 
			if (isset($_POST['submit-menu-image'])) {
				$catImage = isset($_FILES['menu-image-input']) ? $_FILES['menu-image-input'] : '';
				$target_dir = "uploads/menu/";
				$newImgPath = $target_dir . basename($catImage['name']);

				if ($catImage["size"] <=500000) {
					if (move_uploaded_file($catImage["tmp_name"], $newImgPath)) {
						?>
						<script>
							$('#modal-menu-added').modal('toggle')
						</script>
						<?php
					}
				}

				if ($stmt = $mysqli->prepare("INSERT INTO menu (imgPath) VALUES (?)")) {
					$stmt->bind_param("s", $newImgPath);
					$stmt->execute();
				}
				$stmt->close();
			}
		} else {
				unset($_SESSION['logged_in']);
				header('Location: login.php');
			}
	} else {
		header('Location: login.php');
	}
?>