<!DOCTYPE html>
<html lang="en">
<?php
    session_start();
    include 'include/connect.php';

?>
<head>

<?php
    include 'include/meta.php';
?>

    <title>Meows</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/1-col-portfolio.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/styles.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php
    include 'include/navbar.php';
?>

<?php
    $result="";
    $result = $mysqli->query("SELECT id, name, description, isAdopted, imgPath FROM cat WHERE isAdopted = '1'");
    $name="";
    $description="";
    $imgPath="";

    if ($result->num_rows === 0) {
        echo '<div class="container">';
            echo '<div class="box">';
                echo '<h2 class="heading-text text-center">No results found</h2>';
            echo '</div>';
        echo '</div>';
    } else{
        while ($row = $result->fetch_assoc()) {

            $id=$row['id'];
            $name=$row['name'];
            $description=$row['description'];
            $imgPath=$row['imgPath'];

            $link_address='adoptionForm.php';

            echo '<div class="container">';
                echo '<div class="row">';
                    echo '<div class="box">';
                        echo '<div class="col-lg-8 col-md-6">';
                            echo '<a href="#">';
                                echo '<img class="img-responsive" src="'.$imgPath.'" alt="">';
                            echo '</a>';
                        echo '</div>';

                        echo '<div class="col-lg-4 col-md-6">';
                            echo '<h3 class="intro-text">';
                            echo "Cat Name: $name";
                            echo '</h3>';

                            echo '<p>';
                            echo "Cat Description: $description";
                            echo '</p>';

                        echo '</div>';

                    echo '</div>';
                echo '</div>';
            echo '</div>';
        }
    }
?>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; MyKittyCafe 2016</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
