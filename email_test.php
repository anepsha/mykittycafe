<?php
	if(isset($_POST['btn-submit'])){
$to      = 'nobody@example.com';
$subject = 'the subject';
$message = 'hello';
$headers = 'From: webmaster@example.com' . "\r\n" .
    'Reply-To: webmaster@example.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);

		$firstName = isset($_POST['input-first-name']) ? htmlentities($_POST['input-first-name'], ENT_QUOTES) : '';
		$lastName = isset($_POST['input-last-name']) ? htmlentities($_POST['input-last-name'], ENT_QUOTES) : '';
		$email = isset($_POST['input-email']) ? htmlentities($_POST['input-email'], ENT_QUOTES) : '';
		$phone = isset($_POST['input-phone']) ? htmlentities($_POST['input-phone'], ENT_QUOTES) : '';
		$comments = isset($_POST['input-comments']) ? htmlentities($_POST['input-comments'], ENT_QUOTES) : '';
					
		require 'phpmailer/PHPMailerAutoload.php';
		
		$adminMail = new PHPMailer;
		
		/* change setFrom in production */
		$adminMail->setFrom('from@example.com', 'MyKittyCafe');
		$adminMail->addAddress($email, $firstName." ".$lastName);
		$resume_file;
		$cover_letter_file;
		
		
		
		$adminMail->isHTML(true);
		$adminMail->Subject = 'Volunteer Application For From '.$firstName.' '.$lastName;
		
		/* will need to update email design later */
		$adminMail->Body    = "
			<strong>
				Title
			</strong>asdf<br/>
			<strong>
				Description
			</strong>asdfadsf<br/>
			<br/>
			<strong>Name
			</strong>".$firstName." ".$lastName."
		";
		$adminMail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if($adminMail->send()) {								
			$_SESSION['volunteerApplicationSubmitted'] = true;
			header('Location: volunteer.php');
		} else {
			header('Location: volunteer.php');
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<form action='' method='post' class='form-horizontal' name='frm-volunteer-apply' id='frm-volunteer-apply' enctype='multipart/form-data'>
							<div class='form-group'>
								<label for='input-first-name' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									First Name
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<input type='text' class='form-control' name='input-first-name' id='input-first-name'/>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-last-name' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									Last Name
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<input type='text' class='form-control' name='input-last-name' id='input-last-name'/>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-email' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									Email
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<input type='text' class='form-control' name='input-email' id='input-email'/>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-phone' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									Phone
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<input type='text' class='form-control' name='input-phone' id='input-phone'/>
								</div>
							</div>
								<div class='form-group'>
									<label for='input-resume' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										Resume
									</label>
									<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										<input type="file" name='input-resume' id="input-resume">
										<p class="help-block">.pdf, .doc, or .docx only.</p>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-cover-letter' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										Cover Letter
									</label>
									<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										<input type="file" name='input-cover-letter' id="input-cover-letter">
										<p class="help-block">.pdf, .doc, or .docx only.</p>
									</div>
								</div>
							<div class='form-group'>
								<label for='input-message' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
								Questions, Comments
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<textarea class='form-control' name='input-message' id='input-message' rows='3'>
									</textarea>
								</div>
							</div>
							<div class='form-group'>
								<div class='col-xs-12 col-sm-push-6 col-md-push-6 col-lg-push-6'>
								</div>
							</div>
							<div class='form-group'>
								<div class='col-xs-12 col-sm-push-6 col-md-push-6 col-lg-push-6'>
									<button type='submit' class='btn btn-success' name='btn-submit' id='btn-submit'>
										Submit Application
									</button>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	<?php
		include 'include/js.php';
	?>
	</body>
</html>