<?php
	session_start();
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="box">
					<div class="col-xs-12 text-center">
						<div class="btn-group-vertical" role="group" aria-label="...">
							<a href='uploadMenuCMS.php' class='btn btn-default'>
								Upload Menu
							</a>
							<a href='specialtyItemsCMS.php' class='btn btn-default'>
								Specialty Items
							</a>
							<a href='events.php' class='btn btn-default'>
								Events
							</a>
							<a href='addCatsListing.php' class='btn btn-default'>
								Cats
							</a>
							<a href='viewSpecials.php' class='btn btn-default'>
								Specials
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	<?php
		include 'include/js.php';
	?>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>