<?php
	session_start();
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			include 'include/connect.php';
			
			$action = isset($_GET['action']) ? htmlentities($_GET['action'], ENT_QUOTES) : 'view' ;
			$id = isset($_GET['id']) ? htmlentities($_GET['id'], ENT_QUOTES) : 0 ;
			
			switch($action){
				case 'view':
					break;
				case 'publish':
					$publish = 1;
					$stmt = "
						UPDATE 
							`special` 
						SET 
							`publish`=?
						WHERE id = ?
					";
					
					if($sql = $mysqli->prepare($stmt)){
						$sql->bind_param("ii", $publish, $id);
						$sql->execute();
						$sql->close();
						
						header('Location: viewSpecials.php');
					}
					
					break;
				case 'unpublish':
					$publish = 0;
					$stmt = "
						UPDATE 
							`special` 
						SET 
							`publish`=?
						WHERE id = ?
					";
					
					if($sql = $mysqli->prepare($stmt)){				
						$sql->bind_param("ii", $publish, $id);
						$sql->execute();
						$sql->close();
						
						header('Location: viewSpecials.php');
					}
					
					break;
			}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="small-box">
					<div class="col-lg-12">
						<hr>
						<h2 class="heading-text text-center">Specials</h2>
						<hr>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<a href='createSpecials.php?action=create' class='btn btn-default pull-right' style='margin-bottom: 1.5em;'>
							Create Special
						</a>
						<table class='table table-striped'>
								<tr>
									<th>
										Title
									</th>
									<th>
										Description
									</th>
									<th>
										Date Start
									</th>
									<th>
										Date End
									</th>
									<td>
									</td>
								</tr>
					<?php
						$stmt = "
							SELECT 
								`id`, 
								`name`, 
								`description`, 
								`dateStart`, 
								`dateEnd`, 
								`publish`
							FROM 
								`special`
						";
						
						if($sql = $mysqli->prepare($stmt)){
							$sql->execute();
							$res = $sql->get_result();
							
							while($row = $res->fetch_assoc()){
					?>
							<tr>
								<td>
								<?php
									echo $row['name'];
								?>
								</td>
								<td>
								<?php
									echo $row['description'];
								?>
								</td>
								<td>
								<?php
									echo $row['dateStart'];
								?>
								</td>
								<td>
								<?php
									echo $row['dateEnd'];
								?>
								</td>
								<td>
									<a href='createSpecials.php?action=update&id=<?php echo $row['id']; ?>' class='btn btn-default'>Update</a>
									<?php
										if($row['publish']){
									?>
										<a href='viewSpecials.php?action=unpublish&id=<?php echo $row['id']; ?>' class='btn btn-default'>Unpublish</a>
									<?php
										} else {
									?>
										<a href='viewSpecials.php?action=publish&id=<?php echo $row['id']; ?>' class='btn btn-default'>Publish</a>
									<?php
										}
									?>
								</td>
							</tr>
					<?php
							}
						}
					?>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	<?php
		include 'include/js.php';
	?>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>
