<?php
	session_start();
	include 'include/connect.php';
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			if(isset($_POST['btn-delete'])){
				$action = isset($_POST['input-action']) ? htmlentities($_POST['input-action'], ENT_QUOTES) : '';
				$id = isset($_POST['input-id']) ? htmlentities($_POST['input-id'], ENT_QUOTES) : 0;
				$stmt = "
					DELETE FROM 
						`event` 
					WHERE 
						id = ?
				";
				$sql = $mysqli->prepare($stmt);
				$sql->bind_param("i", $id);
				$sql->execute();
				
				header('Location: events.php');
			}
			
			$action = isset($_GET['action']) ? htmlentities($_GET['action'], ENT_QUOTES) : '';
			$id = isset($_GET['id']) ? htmlentities($_GET['id'], ENT_QUOTES) : 0;
				
			switch($action){
				case 'update':
					/* do something */
					break;
				case 'approve':
					/* do something */
					$isApproved = 1;
					$stmt = "
						UPDATE 
							`event` 
						SET 
							`isApproved`=?
						WHERE 
							id = ?
					";
					$sql = $mysqli->prepare($stmt);
					$sql->bind_param("ii", $isApproved, $id);
					$sql->execute();
					
					header('Location: events.php');
					break;
				case 'disapprove':
					/* do something */
					$isApproved = 0;
					$stmt = "
						UPDATE 
							`event` 
						SET 
							`isApproved`=?
						WHERE 
							id = ?
					";
					$sql = $mysqli->prepare($stmt);
					$sql->bind_param("ii", $isApproved, $id);
					$sql->execute();
					
					header('Location: events.php');
					break;
			}
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
	?>
    <!-- /.container -->
	<div class="container">
	<br/>
		<div class="row">
			<div class="box">
				<a href='createEvents.php?action=create' class='btn btn-default pull-right'>
					Create Event
				</a><br/>
				<br/>
				<table class='table table-striped'>
					<tr>
						<th>Title</th>
						<th>Description</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Location</th>
						<th>Event Type</th>
						<td>
						</td>
					</tr>
					<?php
						$stmt = "
							SELECT 
								e.`id`, 
								`title`, 
								e.`description`, 
								`dateStart`, 
								`dateEnd`, 
								`isApproved`, 
								`eventTypeId`, 
								`location`,
								name
							FROM 
								`event` e
								JOIN
									eventtype et
								ON
									e.eventTypeId = et.id
						";
						$sql = $mysqli->prepare($stmt);
						$sql->execute();
						$res = $sql->get_result();
						
						while($row = $res->fetch_assoc()){
					?>
					<tr>
						<td>
						<?php
							echo $row['title'];
						?>
						</td>
						<td>
						<?php
							echo $row['description'];
						?>
						</td>
						<td>
						<?php
							echo $row['dateStart'];
						?>
						</td>
						<td>
						<?php
							echo $row['dateEnd'];
						?>
						</td>
						<td>
						<?php
							echo $row['location'];
						?>
						</td>
						<td>
						<?php
							echo $row['name'];
						?>
						</td>
						<td>
							<form action='' method='post' name='frm-delete' id='frm-delete' style='display: inline;'>
								<input type='submit' class='btn btn-primary' name='btn-delete' id='btn-delete' value='Delete'/>
								<input type='hidden' name='input-action' id='input-action' value='delete'/>
								<input type='hidden' name='input-id' id='input-id' value='<?php echo $row['id'];?>'/>
							</form>
							<!-- <a href='events.php?action=delete&id=<?php echo $row['id']; ?>' class='btn btn-primary'>Delete</a> -->
							<a href='createEvents.php?action=update&id=<?php echo $row['id']; ?>' class='btn btn-primary'>Update</a>
							<?php
								if($row['isApproved']){
							?>
								<a href='events.php?action=disapprove&id=<?php echo $row['id']; ?>' class='btn btn-primary'>Disapprove</a>
							<?php
								} else {
							?>
								<a href='events.php?action=approve&id=<?php echo $row['id']; ?>' class='btn btn-primary'>Approve</a>
							<?php							
								}
							?>
						</td>
					</tr>
					<?php
						}
					?>
				</table>
			</div>
		</div>
	</div>
	<?php
		include 'include/js.php';
	?>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/additional-methods.min.js"></script>
	<script src="js/events.js"></script>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>
