<?php
	session_start();
	include 'include/connect.php';
	
	$volunteerOpportunityId = isset($_GET['id']) ? htmlentities($_GET['id'], ENT_QUOTES) : header('Location: volunteer.php');
	$volunteerOpportunityExists = false;
	$stmt = "
		SELECT 
			title, 
			description, 
			dateStart, 
			dateEnd, 
			hoursPerWeek, 
			contactName, 
			contactEmail, 
			contactPhone, 
			location, 
			requireResume, 
			requireCoverLetter 
		FROM 
			volunteeropportunity 
		WHERE 
			id = ".$volunteerOpportunityId."
	";
	$res = $mysqli->query($stmt);
	$row;
	
	if($res->num_rows == 1){
		$volunteerOpportunityExists = true;
		$row = $res->fetch_assoc();
	}
	
	/* development key */
	$siteKey = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI';
	
	if(isset($_POST['btn-submit'])){
		if(isset($_POST['g-recaptcha-response'])){
			include 'recaptcha/autoload.php';
			
			/* development key */
			$secret = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';
			
			$recaptcha = new \ReCaptcha\ReCaptcha($secret);
			$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

			if ($resp->isSuccess()){
				/* get form inputs */
				$firstName = isset($_POST['input-first-name']) ? htmlentities($_POST['input-first-name'], ENT_QUOTES) : '';
				$lastName = isset($_POST['input-last-name']) ? htmlentities($_POST['input-last-name'], ENT_QUOTES) : '';
				$email = isset($_POST['input-email']) ? htmlentities($_POST['input-email'], ENT_QUOTES) : '';
				$phone = isset($_POST['input-phone']) ? htmlentities($_POST['input-phone'], ENT_QUOTES) : '';
				$comments = isset($_POST['input-comments']) ? htmlentities($_POST['input-comments'], ENT_QUOTES) : '';
								
				/* save applicant info to db */
				$stmt = "
					INSERT INTO 
						applicant(
							firstName, 
							lastName, 
							email, 
							phoneNumber
						) 
					VALUES (
						?,
						?,
						?,
						?
					)
				";
								
				if($sql = $mysqli->prepare($stmt)){
					$sql->bind_param("ssss", $firstName, $lastName, $email, $phone);
					
					if($sql->execute()){
						/* save record of application */
						$applicantId = $sql->insert_id;
						$stmt = "
							INSERT INTO 
								volunteerapplication(
									volunteerOpportunityId, 
									applicantId
								) 
							VALUES (
								".$volunteerOpportunityId.",
								".$applicantId."
							)
						";
						
						if($mysqli->query($stmt)){
							/* send volunteer application to admin */
							require 'phpmailer/PHPMailerAutoload.php';
							
							$adminMail = new PHPMailer;
							
							/* change setFrom in production */
							$adminMail->setFrom('from@example.com', 'MyKittyCafe');
							$adminMail->addAddress($email, $firstName." ".$lastName);
							$resume_file;
							$cover_letter_file;
							
							/* validate and attach resume upload */
							if($row['requireResume']){
								$resume = $_FILES['input-resume'];
								$target_dir = "uploads/volunteer/";
								$resume_file = $target_dir . basename($resume["name"]);
								
								// Check file size
								if ($resume["size"] <= 5000000) {
									if (move_uploaded_file($resume["tmp_name"], $resume_file)) {
										$adminMail->addAttachment($resume_file); 
									} else {
										header("Location: volunteer-apply.php?id=".$volunteerOpportunityId);
									}
								} else {
									header("Location: volunteer-apply.php?id=".$volunteerOpportunityId);
								}
							}
							
							/* validate and attach cover letter upload */
							if($row['requireCoverLetter']){
								$cv = $_FILES['input-cover-letter'];
								$target_dir = "uploads/volunteer/";
								$cover_letter_file = $target_dir . basename($cv["name"]);
								
								// Check file size
								if ($cv["size"] <= 5000000) {
									if (move_uploaded_file($cv["tmp_name"], $cover_letter_file)) {
										$adminMail->addAttachment($cover_letter_file); 
									} else {
										header("Location: volunteer-apply.php?id=".$volunteerOpportunityId);
									}
								} else {
									header("Location: volunteer-apply.php?id=".$volunteerOpportunityId);
								}
							}
							
							$adminMail->isHTML(true);
							$adminMail->Subject = 'Volunteer Application For '.$row['title'].' From '.$firstName.' '.$lastName;
							
							/* will need to update email design later */
							$adminMail->Body    = "
								<strong>
									Title
								</strong>".$row['title']."<br/>
								<strong>
									Description
								</strong>".$row['description']."<br/>
								<br/>
								<strong>Name
								</strong>".$firstName." ".$lastName."
							";
							$adminMail->AltBody = 'This is the body in plain text for non-HTML mail clients';

							if($adminMail->send()) {								
								$_SESSION['volunteerApplicationSubmitted'] = true;
								header('Location: volunteer.php');
							} else {
								header("Location: volunteer-apply.php?id=".$volunteerOpportunityId);
							}
													
							/* send confirmation email to user */
							
						} else {
							header("Location: volunteer-apply.php?id=".$volunteerOpportunityId);
						}
					}
				} else {
					header("Location: volunteer-apply.php?id=".$volunteerOpportunityId);
				}
			} else {
				header("Location: volunteer-apply.php?id=".$volunteerOpportunityId);
			}
		} else {
			header("Location: volunteer-apply.php?id=".$volunteerOpportunityId);
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		if($volunteerOpportunityExists){
	?>
		<div class="modal fade" tabindex="-1" role="dialog" id='modal-volunteer-apply'>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
						<?php
							echo $row['title'];
						?>
						</h4>
					</div>
					<div class="modal-body">
						<p>
							To volunteer, please submit the application form below. In addition, you must also meet the following requirements:<br/>
							<ul>
								<li>
									Be 14 years or older.
								</li>
								<li>
									A self-starter.
								</li>
								<li>
									Willing to follow written list-base instruction without supervision.
								</li>
							</ul>
						</p><br/>
						<form action='' method='post' class='form-horizontal' name='frm-volunteer-apply' id='frm-volunteer-apply' enctype='multipart/form-data'>
							<div class='form-group'>
								<label for='input-first-name' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									First Name
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<input type='text' class='form-control' name='input-first-name' id='input-first-name'/>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-last-name' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									Last Name
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<input type='text' class='form-control' name='input-last-name' id='input-last-name'/>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-email' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									Email
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<input type='text' class='form-control' name='input-email' id='input-email'/>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-phone' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									Phone
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<input type='text' class='form-control' name='input-phone' id='input-phone'/>
								</div>
							</div>
							<?php
								if($row['requireResume']){
							?>
								<div class='form-group'>
									<label for='input-resume' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										Resume
									</label>
									<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										<input type="file" name='input-resume' id="input-resume">
										<p class="help-block">.pdf, .doc, or .docx only.</p>
									</div>
								</div>
							<?php
								}
								
								if($row['requireCoverLetter']){
							?>
								<div class='form-group'>
									<label for='input-cover-letter' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										Cover Letter
									</label>
									<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										<input type="file" name='input-cover-letter' id="input-cover-letter">
										<p class="help-block">.pdf, .doc, or .docx only.</p>
									</div>
								</div>
							<?php
								}
							?>
							<div class='form-group'>
								<label for='input-message' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
								Questions, Comments
								</label>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
									<textarea class='form-control' name='input-message' id='input-message' rows='3'>
									</textarea>
								</div>
							</div>
							<div class='form-group'>
								<div class='col-xs-12 col-sm-push-6 col-md-push-6 col-lg-push-6'>
									<div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
								</div>
							</div>
							<div class='form-group'>
								<div class='col-xs-12 col-sm-push-6 col-md-push-6 col-lg-push-6'>
									<button type='submit' class='btn btn-success' name='btn-submit' id='btn-submit'>
										Submit Application
									</button>
								</div>
							</div>
							
						</form>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	<?php
		}
		
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
					<?php
						if($volunteerOpportunityExists){
							
					?>
						<h1 class='text-center'>
						<?php
							echo $row['title'];
						?>
						</h1><br/>
						<p class='text-center'>
						<?php
							echo $row['description'];
						?>
						</p><br/>
						<div class='row'>
							<div class='col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2'>
								<table class='table table-striped'>
									<tr>
										<th>
											Duration
										</th>
										<td>
										<?php									
											$dateStart = strtotime($row['dateStart']);
											$dateStartFormatted = $dateStart == 0 ? 'TBD' : date('Y/m/d', $dateStart);
											$dateEnd = strtotime($row['dateEnd']);
											$dateEndFormatted = $dateEnd == 0 ? 'TBD' : date('Y/m/d', $dateEnd);
											echo $dateStartFormatted.' to '.$dateEndFormatted;
										?>
										</td>
									</tr>
									<tr>
										<th>
											Approximate Hours Per Week
										</th>
										<td>
										<?php
											echo $row['hoursPerWeek'];
										?>
										</td>
									</tr>
									<tr>
										<th>
											Contact Information
										</th>
										<td>
										<?php
											echo $row['contactName'];
										?><br/>
											<a href='mailto:<?php echo $row['contactEmail'];?>'>
											<?php
												echo $row['contactEmail'];
											?>
											</a><br/>
											<abbr title='phone'>P:</abbr>
										<?php
											echo $row['contactPhone'];
										?>
										</td>
									</tr>
									<tr>
										<th>
											Location
										</th>
										<td>
										<?php
											echo $row['location'];
										?>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<button type="button" class="btn btn-lg btn-primary center-block" data-toggle="modal" data-target="#modal-volunteer-apply">
							Apply
						</button>
					<?php
						} else {
					?>
						<h2>
							Sorry!
						</h2>
						<p>
							That volunteer opportunity doesn't exist. Please go <a href='volunteer.php'>back</a>.
						</p>
					<?php
						}
					?>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	<?php
		include 'include/js.php';
	?>
		<script src="js/jquery.validate.min.js"></script>
		<script src="js/additional-methods.min.js"></script>
		<script src="js/phoneUS.js"></script>
		<script src="js/filesize.js"></script>
		<script src="https://www.google.com/recaptcha/api.js"></script>
		<script src="js/volunteer-apply.js"></script>
	</body>
</html>
