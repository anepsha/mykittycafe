<?php
	session_start();
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			include 'include/connect.php';
			
			$specialAdded = isset($_SESSION['special_added']) ? $_SESSION['special_added'] ? true : false : false;
			$specialCreated = isset($_SESSION['special_created']) ? $_SESSION['special_created'] ? true : false : false;
			$action = isset($_GET['action']) ? htmlentities($_GET['action'], ENT_QUOTES) : 'create';
			$id = isset($_GET['id']) ? htmlentities($_GET['id'], ENT_QUOTES) : 0;
			$special;
			
			if($action == 'update'){
				$stmt = "
					SELECT 
						`id`
					FROM `special` 
					WHERE 
						id = ?
				";
				
				if($sql = $mysqli->prepare($stmt)){
					$sql->bind_param("i", $id);
					$sql->execute();
					$sql->store_result();
					
					if($sql->num_rows == 1){
						$stmt = "
							SELECT 
								`name`, 
								`description`, 
								`dateStart`, 
								`dateEnd`, 
								`imagePath`, 
								`discount`, 
								`specialType` 
							FROM 
								`special` 
							WHERE 
								id = ?
						";
						$sql = $mysqli->prepare($stmt);
						$sql->bind_param("i", $id);
						$sql->execute();
						$res = $sql->get_result();
						
						if($res->num_rows == 1){
							$special = $res->fetch_assoc();
						}
					}
					
					$sql->close();
				}
			}
			
			if(isset($_POST['btn-submit'])){
				$name = isset($_POST['input-title']) ? $mysqli->real_escape_string($_POST['input-title']) : '';
				$description = isset($_POST['input-description']) ? $mysqli->real_escape_string($_POST['input-description']) : '';
				$discount = isset($_POST['input-discount']) ? $mysqli->real_escape_string($_POST['input-discount']) : '';
				$dateStart = isset($_POST['input-date-start']) ? $mysqli->real_escape_string($_POST['input-date-start']) : '';
				$dateEnd = isset($_POST['input-date-end']) ? $mysqli->real_escape_string($_POST['input-date-end']) : '';
				$imagePath = isset($special['imagePath']) ? $special['imagePath'] : '';
				
				if(isset($_FILES['input-image'])){
					$file_upload = $_FILES['input-image'];
					
					if(strlen($file_upload['name']) > 0){
						$target_dir = "uploads/specials/";
						$basename = basename($file_upload["name"]);
							
						// Check file size
						if ($file_upload["size"] <= 5000000) {
							// Check duplicate file
							$filename = pathinfo($file_upload['name'], PATHINFO_FILENAME);
							$extension = pathinfo($file_upload['name'], PATHINFO_EXTENSION);
							$increment = ''; //start with no suffix

							while(file_exists($target_dir.$filename.$increment.'.'.$extension)) {
								$increment++;
							}
							
							$basename = $filename.$increment.'.'.$extension;
							$image_dir = $target_dir.$basename;
												
							if (move_uploaded_file($file_upload["tmp_name"], $image_dir)) {
								$imagePath = $image_dir;
							} else {
								header("Location: createSpecials.php");
							}
						} else {
							header("Location: createSpecials.php");
						}
					}
				}
				
				// switch on $action (create, update, unpublish, etc) here
				switch($action){
					case 'create':
						$stmt = "
							INSERT INTO 
								`special`(
									`name`, 
									`description`, 
									`dateStart`, 
									`dateEnd`, 
									`imagePath`, 
									`discount`
								) 
							VALUES (
								?,
								?,
								?,
								?,
								?,
								?
							)
						";
						$sql = $mysqli->prepare($stmt);
						$sql->bind_param("ssssss", $name, $description, $dateStart, $dateEnd, $imagePath, $discount);
						$sql->execute();
						$sql->close();
						$_SESSION['special_added'] = true;
						break;
					case 'update':
						$stmt = "
							UPDATE 
								`special` 
							SET 
								`name`=?,
								`description`=?,
								`dateStart`=?,
								`dateEnd`=?,
								`imagePath`=?,
								`discount`=?
							WHERE id = ?
						";
						$sql = $mysqli->prepare($stmt);
						$sql->bind_param("ssssssi", $name, $description, $dateStart, $dateEnd, $imagePath, $discount, $id);
						$sql->execute();
						$sql->close();
						$_SESSION['special_created'] = true;
						break;
				}
				
				header('Location: createSpecials.php');
			}	
?>

<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	</head>
	<body>
		<div class="modal fade" tabindex="-1" role="dialog" id='modal-create-success'>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							Success!
						</h4>
					</div>
					<div class="modal-body">
						<p>
							Special successfully added!
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<div class="modal fade" tabindex="-1" role="dialog" id='modal-update-success'>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							Success!
						</h4>
					</div>
					<div class="modal-body">
						<p>
							Special successfully updated!
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="small-box">
					<div class="col-lg-12">
						<hr>
						<h2 class="heading-text text-center">Specials</h2>
						<hr>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<form action='' method='post' class='form-horizontal' enctype="multipart/form-data" name='frm-create-special' id='frm-create-special'>
							<div class='form-group'>
								<label for='input-title' class='control-label col-xs-6'>
									Title
								</label>
								<div class='col-xs-6'>
									<input type='text' class='form-control' name='input-title' id='input-title' value='<?php isset($special['name']) ? print $special['name'] : ''; ?>'/>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-description' class='control-label col-xs-6'>
									Description
								</label>
								<div class='col-xs-6'>
									<textarea class='form-control' name='input-description' id='input-description' rows='3'><?php isset($special['description']) ? print $special['description'] : ''; ?></textarea>
								</div>
							</div>
							<div class='form-group' id='section-discount'>
								<label for='input-discount' class='control-label col-xs-6'>
									Discount
								</label>
								<div class='col-xs-6'>
									<input type='text' class='form-control' name='input-discount' id='input-discount' value='<?php isset($special['discount']) ? print $special['discount'] : ''; ?>'/>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-date-start' class='control-label col-xs-6'>
									Date Start
								</label>
								<div class='col-xs-6'>
									<div class='input-group date' id='picker-date-start'>
										<input type='text' class='form-control' name='input-date-start' id='input-date-start' value='<?php isset($special['dateStart']) ? print $special['dateStart'] : ''; ?>'/>
										<span class='input-group-addon'>
											<span class='glyphicon glyphicon-calendar'></span>
										</span>
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-date-end' class='control-label col-xs-6'>
									Date End
								</label>
								<div class='col-xs-6'>
									<div class='input-group date' id='picker-date-end'>
										<input type='text' class='form-control' name='input-date-end' id='input-date-end' value='<?php isset($special['dateEnd']) ? print $special['dateEnd'] : ''; ?>'/>
										<span class='input-group-addon'>
											<span class='glyphicon glyphicon-calendar'></span>
										</span>
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label for='input-image' class='control-label col-xs-6'>
									Image
								</label>
								<div class='col-xs-6'>
									<input type="file" name='input-image' id="input-image">
									<p class="help-block">.jpg, .png, .gif only.</p>
								</div>
							</div>
							<div class='form-group'>
								<div class='col-xs-6 col-xs-push-6'>
									<input type='submit' class='btn btn-success' name='btn-submit' id='btn-submit' value='Create'/>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	<?php
		include 'include/js.php';
	?>
		<script src="js/jquery.validate.min.js"></script>
		<script src="js/additional-methods.min.js"></script>
		<script src="js/filesize.js"></script>
		<script src="js/moment.js"></script>
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<script src="js/createSpecials.js"></script>
		<?php
			if($specialAdded){
		?>
		<script>
			$('#modal-create-success').modal('toggle')
		</script>
		<?php
				$_SESSION['special_added'] = false;
			}
			
			if($specialCreated){
		?>
		<script>
			$('#modal-update-success').modal('toggle')
		</script>
		<?php
				$_SESSION['special_created'] = false;
			}
		?>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>