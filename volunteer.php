<?php
	session_start();
	include 'include/connect.php';
	
	$pageTitle = 'Volunteer';
	$volunteerApplicationSubmitted = isset($_SESSION['volunteerApplicationSubmitted']) ? $_SESSION['volunteerApplicationSubmitted'] ? true : false : false;
		
	/* development key */
	$siteKey = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI';
	
	if(isset($_POST['btn-submit'])){
		if(isset($_POST['g-recaptcha-response'])){
			include 'recaptcha/autoload.php';
			
			/* development key */
			$secret = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';
			
			$recaptcha = new \ReCaptcha\ReCaptcha($secret);
			$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

			if ($resp->isSuccess()){
				/* get form inputs */
				$firstName = isset($_POST['input-first-name']) ? htmlentities($_POST['input-first-name'], ENT_QUOTES) : '';
				$lastName = isset($_POST['input-last-name']) ? htmlentities($_POST['input-last-name'], ENT_QUOTES) : '';
				$email = isset($_POST['input-email']) ? htmlentities($_POST['input-email'], ENT_QUOTES) : '';
				$phone = isset($_POST['input-phone']) ? htmlentities($_POST['input-phone'], ENT_QUOTES) : '';
								
				/* save applicant info to db */
				$stmt = "
					INSERT INTO 
						applicant(
							firstName, 
							lastName, 
							email, 
							phoneNumber
						) 
					VALUES (
						?,
						?,
						?,
						?
					)
				";
								
				if($sql = $mysqli->prepare($stmt)){
					$sql->bind_param("ssss", $firstName, $lastName, $email, $phone);
					
					if($sql->execute()){
						require 'phpmailer/PHPMailerAutoload.php';
						
						$adminMail = new PHPMailer;
						
						/* change setFrom in production */
						$adminMail->setFrom('from@example.com', 'MyKittyCafe');
						$adminMail->addAddress($email, $firstName." ".$lastName);
						$adminMail->isHTML(true);
						$adminMail->Subject = 'Volunteer Application From '.$firstName.' '.$lastName;
						
						/* will need to update email design later */
						$adminMail->Body    = "
							<strong>
								Volunteer Application
							</strong><br/>
							<strong>Name
							</strong>".$firstName." ".$lastName."
						";
						$adminMail->AltBody = 'This is the body in plain text for non-HTML mail clients';

						if($adminMail->send()) {								
							$_SESSION['volunteerApplicationSubmitted'] = true;
						}
												
						header('Location: volunteer.php');
						/* send confirmation email to user */
					}
				} else {
					header('Location: volunteer.php');
				}
			} else {
				header('Location: volunteer.php');
			}
		} else {
			header('Location: volunteer.php');
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>
		<?php
			echo $pageTitle.' - My Kitty Cafe';
		?>
		</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/volStyles.css">
	</head>
	<body>
		<div class="modal fade" tabindex="-1" role="dialog" id='modal-application-success'>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							Success!
						</h4>
					</div>
					<div class="modal-body">
						<p>
							Thank you for your volunteer application. You should receive a confirmation e-mail shortly within the next 24 hours.<br/>
							<br/>
							If you do not, please check your Junk or Spam e-mail folder as it may have been mistaken as Junk or Spam e-mail.
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="small-box">
					<div class="col-lg-12">
						<hr>
						<h2 class="heading-text text-center">Volunteer
						</h2>
						<hr>
						<hr class="visible-xs">
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<div class="row">
							<p class='text-center'>
								To volunteer, please submit the application form below. MKC (MyKittyCafe) staff will contact you as soon as possible.
							</p><br/>
							<form action='' method='post' class='form-horizontal' name='frm-volunteer-apply' id='frm-volunteer-apply'>
								<div class='form-group'>
									<label for='input-first-name' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										First Name
									</label>
									<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										<input type='text' class='form-control' name='input-first-name' id='input-first-name'/>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-last-name' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										Last Name
									</label>
									<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										<input type='text' class='form-control' name='input-last-name' id='input-last-name'/>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-email' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										Email
									</label>
									<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										<input type='text' class='form-control' name='input-email' id='input-email'/>
									</div>
								</div>
								<div class='form-group'>
									<label for='input-phone' class='control-label col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										Phone
									</label>
									<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
										<input type='text' class='form-control' name='input-phone' id='input-phone'/>
									</div>
								</div>
								<div class='form-group'>
									<div class='col-xs-12 col-sm-push-6 col-md-push-6 col-lg-push-6'>
										<div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
									</div>
								</div>
								<div class='form-group'>
									<div class='col-xs-12 col-sm-push-6 col-md-push-6 col-lg-push-6'>
										<button type='submit' class='btn btn-success' name='btn-submit' id='btn-submit'>
											Submit Application
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	<?php
		include 'include/js.php';
	?>
		<script src="js/jquery.validate.min.js"></script>
		<script src="js/additional-methods.min.js"></script>
		<script src="js/phoneUS.js"></script>
		<script src="https://www.google.com/recaptcha/api.js"></script>
		<script src="js/volunteer-apply.js"></script>
	<?php
		if($volunteerApplicationSubmitted){
	?>
		<script>
			$('#modal-application-success').modal('toggle')
		</script>
	<?php
			$_SESSION['volunteerApplicationSubmitted'] = false;
		}
	?>
	</body>
</html>
