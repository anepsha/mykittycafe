<?php
	session_start();
	include 'include/connect.php';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
			<div class="row">
				<div class="small-box">
					<div class="col-lg-12">
						<hr>
						<h2 class="heading-text text-center">Specials</h2>
						<hr>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<!-- enter page content here -->
						<?php
							$stmt = "
								SELECT 
									s.`name`, 
									s.`description`, 
									`dateStart`, 
									`dateEnd`, 
									`imagePath`, 
									`discount`,
									`publish`
								FROM `special` s
								WHERE 
									publish = 1
							";
							if($sql = $mysqli->prepare($stmt)){
								$sql->execute();
								$res = $sql->get_result();
								$row_cn = $res->num_rows;

								if ($row_cn == 0) {
									echo '<h2 class="heading-text text-center">No specials found</h2>';
								}
								
								while($row = $res->fetch_assoc()){
									$dateStart = strtotime($row['dateStart']);
									$dateStartDisplay = $dateStart == 0 ? '' : date('Y/m/d', $dateStart);
									$dateEnd = strtotime($row['dateEnd']);
									$dateEndDisplay = $dateEnd == 0 ? '' : date('Y/m/d', $dateEnd);
									
						?>
						<div class="panel panel-default">
							<div class="panel-body">
								<h2 class='text-center'>
								<?php
									echo $row['name'];
								?>
								</h2><br/>
								<?php
									if(strlen($row['imagePath']) > 0){
								?>
								<img src="<?php echo $row['imagePath']; ?>" class='center-block img-responsive' style='margin: 0 auto'/><br/>
								<?php
									}
								?>
								<p class='text-center'>
								<?php
									echo $row['description'];
								?><br/>
								<?php
									$valid_dates_display = '';
									
									if(strlen($dateStartDisplay) > 0){
										$valid_dates_display .= "This offer is valid from ".$dateStartDisplay;
									}
									
									if(strlen($dateEndDisplay) > 0){
										$valid_dates_display .= " to ".$dateEndDisplay;
									}
									
									echo $valid_dates_display;
								?>
								</p>
							</div>
						</div>
						<?php
								}
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	<?php
		include 'include/js.php';
	?>
	</body>
</html>
