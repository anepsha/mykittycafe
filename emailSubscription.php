<?php
	session_start();
	include 'include/connect.php';
	$pageTitle = 'Email Subscription';
		
	if($mysqli){
		$email = isset($_POST['email-subscription']) ? $_POST['email-subscription'] : '';
		$sql_store = "INSERT into emaillist (email) VALUES ('$email')";
		$query = mysqli_query($mysqli,$sql_store);
		
		if(mysqli_query($mysqli,$sql_store)){
			echo "the email .$email. is added";
		} else {
			error_log('problem inserting record');
		}
	}else{
			die(mysql_error());
	}
		
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
		include 'include/meta.php';
	?>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/1-col-portfolio.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Navigation -->
  <?php
		include 'include/navbar.php';
	?>
    <!-- /.container -->
    <div class="container">
        <div class="row">
            <div class="ccol-lg-12">
                <h1 class="page-header">Subsribe for the latest sales and promotions</h1>
                 <div class="well">
                     <form action="emailSubscription.php" method="POST" name='frm-email-subscription' id='frm-email-subscription'>
                      <fieldset class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" type="required" class="form-control" id="email-subscription" name="email-subscription" placeholder="Enter email">
                        <small class="text-muted">We'll never share your email with anyone else.</small>
                      </fieldset>
                      <input type="submit" name="submit" value="Subscribe"/>
                     </form>
                 </div>
        </div>
    </div>
    </div>
	<?php
		include 'include/js.php';
	?>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/additional-methods.min.js"></script>
	<script src="js/emailSubscription.js"></script>
</body>
</html>
