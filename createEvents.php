<?php
	session_start();
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			include 'include/connect.php';
			
			$pageTitle = 'Create an Event';
			$eventAdded = isset($_SESSION['event_added']) ? $_SESSION['event_added'] ? true : false : false;
			$eventCreated = isset($_SESSION['event_created']) ? $_SESSION['event_created'] ? true : false : false;
			$eventType = array(
				'mykittycafe' => 1,
				'community' => 2
			);
			$action = isset($_GET['action']) ? htmlentities($_GET['action'], ENT_QUOTES) : 'create';
			$id = isset($_GET['id']) ? htmlentities($_GET['id'], ENT_QUOTES) : 0;
			$event;

			if ($action == 'update') {
					$stmt = "SELECT id FROM event WHERE id=?";

					if ($sql= $mysqli->prepare($stmt)) {
						$sql->bind_param("i", $id);
						$sql->execute();
						$sql->store_result();

						if ($sql->num_rows == 1) {
							$stmt = "SELECT title, description, dateStart, dateEnd, imagePath, isApproved, eventTypeId, location FROM event WHERE id=?";
							$sql = $mysqli->prepare($stmt);
							$sql->bind_param("i", $id);
							$sql->execute();
							$res = $sql->get_result();

							if ($res->num_rows == 1) {
								$event = $res->fetch_assoc();
							}
						}
						$sql->close();
					}
				}	


			if(isset($_POST['btn-submit'])){
				$title = isset($_POST['event-title']) ? htmlentities($_POST['event-title'], ENT_QUOTES) : '';
				$description = isset($_POST['event-description']) ? $_POST['event-description'] : '';
				$dateStart = isset($_POST['input-date-start']) ? $_POST['input-date-start'] : '';
				$dateEnd = isset($_POST['input-date-end']) ? $_POST['input-date-end'] : '';
				$location = isset($_POST['location']) ? $_POST['location'] : '';
				$eventType = isset($_POST['event-type']) ? $_POST['event-type'] : '';
				$imagePath = isset($event['imagePath']) ? $event['imagePath'] : '';
				$isApproved = 1;
				
				// UPLOAD IMAGE TO SERVER FOLDER
				if(isset($_FILES['upload-image'])){
					$file_upload = $_FILES['upload-image'];
					
					if(strlen($file_upload['name']) > 0){
						$target_dir = "uploads/event/";
						$basename = basename($file_upload["name"]);
							
						// Check file size
						if ($file_upload["size"] <= 5000000) {
							// Check duplicate file
							$filename = pathinfo($file_upload['name'], PATHINFO_FILENAME);
							$extension = pathinfo($file_upload['name'], PATHINFO_EXTENSION);
							$increment = ''; //start with no suffix

							while(file_exists($target_dir.$filename.$increment.'.'.$extension)) {
								$increment++;
							}
							
							$basename = $filename.$increment.'.'.$extension;
							$image_dir = $target_dir.$basename;
												
							if (move_uploaded_file($file_upload["tmp_name"], $image_dir)) {
								$imagePath = $image_dir;
							} else {
								header("Location: createEvents.php");
							}
						} else {
							header("Location: createEvents.php");
						}
					}
				}	
				
				// switch on $action (create, update, unpublish, etc) here
				switch ($action) {
					case 'create':

						$stmt = "
							INSERT INTO 
								`event`( 
									`title`, 
									`description`, 
									`dateStart`, 
									`dateEnd`, 
									`imagePath`, 
									`isApproved`, 
									`eventTypeId`, 
									`location`
								) 
							VALUES (
									?,
									?,
									?,
									?,
									?,
									?,
									?,
									?
								)
						";
						$sql = $mysqli->prepare($stmt);
						$sql->bind_param("sssssiis", $title, $description, $dateStart, $dateEnd, $imagePath, $isApproved, $eventType, $location);
						$sql->execute();
						$sql->close();
						$_SESSION['event_added'] = true;
						break;
					
					case 'update':
						$stmt = "
							UPDATE 
								`event` 
							SET 
								`title`=?,
								`description`=?,
								`dateStart`=?,
								`dateEnd`=?,
								`imagePath`=?,
								`isApproved`=?,
								`eventTypeId`=?,
								`location`=? 
							WHERE id = ?
						";
						$sql = $mysqli->prepare($stmt);
						$sql->bind_param("sssssiisi", $title, $description, $dateStart, $dateEnd, $imagePath, $isApproved, $eventType, $location, $id);
						$sql->execute();
						$sql->close();
						$_SESSION['event_created'] = true;
						break;
				}

				header('Location: createEvents.php');
			}	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>My Kitty Cafe</title>
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.min.css">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
	?>
	<body>
		<div class="modal fade" tabindex="-1" role="dialog" id='modal-create-success'>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							Success!
						</h4>
					</div>
					<div class="modal-body">
						<p>
							Event successfully added!
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<div class="modal fade" tabindex="-1" role="dialog" id='modal-update-success'>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							Success!
						</h4>
					</div>
					<div class="modal-body">
						<p>
							Event successfully updated!
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	<!-- Page Content -->
    <div class="container">
        <!-- Page Heading -->
        <div class="row">
            <div class="small-box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="heading-text text-center">Create an Event</h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="box">
			<div class="col-lg-12">
                <form action="" class="form-horizontal" name="frm-create-event" id="frm-create-event" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-xs-4 col-sm-4" for="event-title">Event Title</label>
						<div class="col-xs-6 col-sm-6">
							<input type="text" class="form-control" id="event-title" name="event-title" placeholder="Event Title" value="<?php isset($event['title']) ? print $event['title'] : '' ?>">
						</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4 col-sm-4" for="event-description">Event Description</label>
						<div class="col-xs-6 col-sm-6">
							<textarea class="form-control" id="event-description" name="event-description" rows="3" placeholder="Event Description"><?php isset($event['description']) ? print $event['description'] : '' ?></textarea>
						</div>
                    </div>
					
                    <div class="form-group">
                       <label for='input-date-start' class='control-label col-xs-4'>Start Date </label>
					   <div class='col-xs-6 col-sm-6'>
						<div class='input-group date' id='picker-date-start' name='picker-date-start'>
							<input type='date' class='form-control' name='input-date-start' id='input-date-start' placeholder='Start Date' value="<?php isset($event['dateStart']) ? print $event['dateStart'] : '' ?>"/>
							<span class='input-group-addon'>
								<span class='glyphicon glyphicon-calendar'></span>
							</span>
						</div>
					   </div>
                    </div>
					
                     <div class="form-group">
                       <label for='input-date-start' class='control-label col-xs-4'>End Date </label>
					   <div class='col-xs-6 col-sm-6'>
						<div class='input-group date' id='picker-date-end'>
							<input type='date' class='form-control' name='input-date-end' id='input-date-end' placeholder='End Date' value="<?php isset($event['dateEnd']) ? print $event['dateEnd'] : '' ?>"/>
							<span class='input-group-addon'>
								<span class='glyphicon glyphicon-calendar'></span>
							</span>
						</div>
					   </div>
                    </div>
						
					<div class="form-group">
                        <label class="control-label col-xs-4 col-sm-4" for="location">Location</label>
						<div class="col-xs-6 col-sm-6">
							<input type="text" class="form-control" id="location" name="location" placeholder="Location" value="<?php isset($event['location']) ? print $event['location'] : '' ?>">
						</div>
                    </div>
					<div class="form-group">
						<label class="control-label col-xs-4 col-sm-4">Event Type</label>
						<div class="col-xs-6 col-sm-6">
						<?php
							$stmt = "
								SELECT 
									`id`, 
									`name`
								FROM `eventtype`
							";
									if($sql = $mysqli->prepare($stmt)){
										$sql->execute();
										$res = $sql->get_result();
										
										while($row = $res->fetch_assoc()){
											if(isset($event) && $row['id'] == $event['eventTypeId']){
								?>
									<div class="radio">
										<label>
											<input type="radio" name="event-type" id="event-type-<?php echo $row['name']; ?>" value="<?php echo $row['id']; ?>" checked>
											<?php echo strtoupper($row['name']); ?>
										</label>
									</div>
								<?php
											} else {
								?>
									<div class="radio">
										<label>
											<input type="radio" name="event-type" id="event-type-<?php echo $row['name']; ?>" value="<?php echo $row['id']; ?>">
											<?php echo strtoupper($row['name']); ?>
										</label>
									</div>
								<?php
											}
										}
									}
								?>
						</div>
					</div>
                    <div class="form-group">
                        <label class="control-label col-xs-4 col-sm-4" for="upload-image">Upload Image</label>
                        <div class="col-xs-6 col-sm-6">
							<input name='upload-image' type="file" placeholder="Browse File.">
						</div>
                    </div>
					<div class="form-group"> 
						<div class="col-sm-offset-4 col-sm-4">
							 <button type="submit" name="btn-submit" class="btn btn-default">Submit</button>
						</div>
					</div>
                </form>
            </div>
			</div>
        </div>
        <!-- /.row -->
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; MyKittyCafe 2016</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->
	<?php
		include 'include/js.php';
	?>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/additional-methods.min.js"></script>
	<script src="js/phoneUS.js"></script>
	<script src="js/moment.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<script src="js/createMyKittyAndCommunityEvent.js"></script>
	<?php
		if($eventAdded){
	?>
	<script>
		$('#modal-create-success').modal('toggle')
	</script>
	<?php
			$_SESSION['event_added'] = false;
		}

		if($eventCreated){
	?>
	<script>
		$('#modal-update-success').modal('toggle')
	</script>
	<?php
			$_SESSION['event_created'] = false;
		}
	?>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>