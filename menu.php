<!DOCTYPE html>
<html lang="en">
<head>
    <?php
		session_start();
		include 'include/connect.php';
		include 'include/meta.php';
	?>

    <title>My Kitty Cafe</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/1-col-portfolio.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php
	include 'include/navbar.php';

	$currentMenuImgPath="";
	$resultsPresented = false;
	if ($stmt = $mysqli->prepare("SELECT imgPath FROM menu ORDER BY id DESC LIMIT 1")) {
		if ($stmt->execute()) {
			$stmt->bind_result($imgPath);
			$stmt->store_result();
			$stmt->fetch();	

			if ($stmt->num_rows > 0) {
				$currentMenuImgPath = $imgPath;
				$stmt->close();
				$resultsPresented = true;

			}		
		}
	}
?>

<div class="container">
	<div class="row">
		<div class="small-box">
			<div class="col-lg-12">
				<hr>
				<h2 class="intro-text text-center">Menu
				</h2>
				<hr>
				<hr class="visible-xs">
			</div>
		</div>
	</div>
</div>
<?php
if ($resultsPresented) {
	?>
<div class="container">
	<div class="row">
		<div class="box">
			<div class="col-lg-12">
				<img src="<?php echo $currentMenuImgPath ?>" class= "img-responsive" style="margin: 0 auto">
			</div>
		</div>
	</div>
</div>
<?php
} else {
?>
<div class="container">
	<div class="row">
		<div class="small-box">
			<div class="col-lg-12">
				<hr>
				<h2 class="heading-text text-center">No menu found
				</h2>
				<hr>
			</div>
		</div>
	</div>
</div>
<?php
}
?>
<div class="container">
	<div class="row">
		<div class="small-box">
			<div class="col-lg-12">
				<hr>
				<h2 class="intro-text text-center">Specialty Items
				</h2>
				<hr>
				<hr class="visible-xs">
			</div>
		</div>
	</div>
</div>

<?php

	$drinks = "Drinks";
	$snacks = "Snacks";
	$food = "Food";

	$name = "";
	$description = "";
	$category = "";
	$imgPath = "";
	$publishedValue = 1;

	if ($stmt = $mysqli->prepare("SELECT * FROM specialtyitems WHERE isPublished = '$publishedValue'")) {
		if ($stmt->execute()) {
			$stmt->store_result();
			if ($stmt->num_rows > 0) {

				?>
	<div class="container">
		<div class='row'>
			<div class="box">
				<div class="col-lg-12">
					<div class="row">
						<div class='col-xs-12 col-sm-4'>
				<?php
				if ($stmt = $mysqli->prepare("SELECT name, category, description, imgPath FROM specialtyitems WHERE category LIKE ? AND isPublished='$publishedValue'")) {
					$stmt->bind_param("s", $drinks);
					if ($stmt->execute()) {
						$stmt->bind_result($name, $category, $description, $imgPath);
						?>
						<h3 class='text-center'><?php echo "$drinks" ?></h3>
						<?php

						while ($stmt->fetch()) {
							echo "<div class='row vertical-align'>";
								echo "<div class='col-xs-6 col-sm-6'>";
									echo "<img src='".$imgPath."' class='img-responsive vcenter pull-right' style='width: 150px; margin-bottom: 10px'/>";
								echo "</div>";

								echo "<div class='col-xs-6 col-sm-6'>";
									echo "<p>";
										echo "<strong>$name</strong></br>";
										echo $description;
									echo "</p>";
								echo "</div>";
							echo "</div>";
						}
					}
					$stmt->free_result();
					$stmt->close();
				}

				echo "</div>";

				echo "<div class='col-xs-12 col-sm-4'>";
				if ($stmt = $mysqli->prepare("SELECT name, category, description, imgPath FROM specialtyitems WHERE category LIKE ? AND isPublished='$publishedValue'")) {
					$stmt->bind_param("s", $snacks);
					if ($stmt->execute()) {

						$stmt->store_result();
						if ($stmt->num_rows > 0) {
							$stmt->bind_result($name, $category, $description, $imgPath);

							echo "<h3 class='text-center' name='drink'>$snacks</h3>";

							while ($stmt->fetch()) {
									echo "<div class='row vertical-align'>";
										echo "<div class='col-xs-6 col-sm-6'>";
											echo "<img src='".$imgPath."' class='img-responsive vcenter pull-right' style='width: 150px'/>";
										echo "</div>";

										echo "<div class='col-xs-6 col-sm-6'>";
											echo "<p>";
												echo "<strong>$name</strong></br>";
												echo $description;
											echo "</p>";
										echo "</div>";
									echo "</div>";
							}		
						} else {
							$snackExist = false;
						}
					}
					$stmt->free_result();
					$stmt->close();
				}
				echo "</div>";

				echo "<div class='col-xs-12 col-sm-4'>";
				if ($stmt = $mysqli->prepare("SELECT name, category, description, imgPath FROM specialtyitems WHERE category LIKE ? AND isPublished= '$publishedValue'")) {
					$stmt->bind_param("s", $food);
					if ($stmt->execute()) {

						$stmt->store_result();
						if ($stmt->num_rows>0) {
							$stmt->bind_result($name, $category, $description, $imgPath);
							echo "<h3 class='text-center'>$food</h3>";

							while ($stmt->fetch()) {
									echo "<div class='row vertical-align'>";
										echo "<div class='col-xs-6 col-sm-6'>";
											echo "<img src='".$imgPath."' class='img-responsive vcenter pull-right' style='width: 150px'/>";
										echo "</div>";

										echo "<div class='col-xs-6 col-sm-6'>";
											echo "<p>";
												echo "<strong>$name</strong></br>";
												echo $description;
											echo "</p>";
										echo "</div>";
									echo "</div>";
							}
						}
						echo "</div>";
					}
					$stmt->close();
				}
				$mysqli->close();
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
			} else {
				?>

				<div class="container">
					<div class="row">
						<div class="small-box">
							<div class="col-lg-12">
								<hr>
								<h2 class="heading-text text-center">No specialty items found
								</h2>
								<hr>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
	}
?>


<?php
	include 'include/js.php';
?>
	
</body>

</html>
