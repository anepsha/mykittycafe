<?php
	session_start();
	include 'include/connect.php';

	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			if (isset($_GET['publish'])) {
				$publishingComplete = false;
				$publishValue = 0;
				$itemId = isset($_GET['publish']) ? (int) $_GET['publish'] : 0;
				if ($stmt = $mysqli->prepare("SELECT isPublished FROM specialtyitems WHERE id=?")) {
					$stmt->bind_param("i", $itemId);
					
					if ($stmt->execute()) {
						$stmt->bind_result($isPublished);
						$stmt->fetch();
						$stmt->close();

						if ($isPublished === 0) {

							$publishValue = 1;

							if ($stmt = $mysqli->prepare("UPDATE specialtyitems SET isPublished = '$publishValue' WHERE id=?")) {
								$stmt->bind_param("i",$itemId);

								if ($stmt->execute()) {
									$stmt->close();
									$publishingComplete = true;
								}
							}
						} else {
							if ($stmt = $mysqli->prepare("UPDATE specialtyitems SET isPublished = '$publishValue' WHERE id=?")) {
								$stmt->bind_param("i",$itemId);

								if ($stmt->execute()) {
									$stmt->close();
									$publishingComplete = true;
								}							
							}
						}	
					}
				}
				if ($publishingComplete) {
					header("Location:specialtyItemsCMS.php");
					exit();
				}
			}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
	?>
		<div class="container">
		<br/>
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<!-- insert content here -->
						<h1 class='text-center'>
							Specialty Items
						</h1>
						<a href='createSpecialtyItemCMS.php' class='btn btn-primary pull-right'>Create Specialty Item</a><br/>
						<br/>
<?php 
		if ($stmt = $mysqli->prepare("SELECT * FROM specialtyitems")) {
			if ($stmt->execute()) {
				$stmt->bind_result($id, $name, $category, $description, $imgPath, $isPublished);
					echo '<table class="table table-stripted">';
						echo '<tr>';
							echo '<th>Category</th>';
							echo '<th>Name</th>';
							echo '<th>Description</th>';
							echo '<th>Image</th>';
						echo '</tr>';
				while ($stmt->fetch()) {
					echo '<tr>';
						echo "<td>$category</td>";
						echo "<td>$name</td>";
						echo "<td>$description</td>";

						echo "<td>";
							echo "<img src=".$imgPath." class='img-responsive' style='width: 150px'>";
						echo "</td>";
					if ($isPublished === 0) {
						echo "<td>";
							echo "<a href='specialtyItemsCMS.php?publish=".$id."' class='btn btn-success' name='publish-specialty-item'>";
								echo "Publish";
							echo "</a>";
						echo "</td>";	
					} else {
						echo "<td>";
							echo "<a href='specialtyItemsCMS.php?publish=".$id."' class='btn btn-warning' name='unpublish-specialty-item'>";
								echo "Unpublish";
							echo "</a>";
						echo "</td>";						
					}

						
						echo "<td>";
							echo "<a href='updateSpecialtyItemCMS.php?id=".$id."' class='btn btn-default' name='update-specialty-item'>";
								echo "Update";
							echo "</a>";
						echo "</td>";
				}
					echo "</table>";
			}
		}
?>
					</div>
				</div>
			</div>
		</div>
	<?php
		include 'include/js.php';
	?>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>
