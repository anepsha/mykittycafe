<?php
	session_start();
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			include 'include/connect.php';
			
			$pageTitle = 'Modify Cat';	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';

	?>
		<title>
		<?php
			echo $pageTitle.' - My Kitty Cafe';
		?>
		</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
		include 'include/addNewCatModal.php';
		include 'include/modifyCatSuccesMes.php';
	?>

<?php
			$catUpdated = false;
			$modifiedImgPath = "";

			if (isset($_GET['cat'])) {

				$cat_id = (int)$_GET['cat'];

				if ($catSelectStmt = $mysqli->prepare("SELECT name, description, imgPath FROM cat WHERE id=?")) {

					$catSelectStmt->bind_param("i", $cat_id);
					$catSelectStmt->execute();
					$catSelectStmt->bind_result($name, $description, $imgPath);
					$catSelectStmt->fetch();
					$catSelectStmt->close();
				}

			}

			if (isset($_POST['modify-cat'])) {


				$catName = isset($_POST['modified-cat-name']) ? $mysqli->real_escape_string($_POST['modified-cat-name']) : '';
				$catDescription = isset($_POST['modified-cat-description']) ? $mysqli->real_escape_string($_POST['modified-cat-description']) : '';

				$catImage = isset($_FILES['cat-new-image-upload']) ? $_FILES['cat-new-image-upload'] : '';
				$modifiedImgPath = $imgPath;

				if (!empty($catImage) && isset($catImage)) {

					$catImage = $_FILES['cat-new-image-upload'];
					$target_dir = "uploads/cats/";
					$newImgPath = $target_dir . basename($catImage['name']);

					if ($catImage["size"] <=500000) {
						if (move_uploaded_file($catImage["tmp_name"], $newImgPath)) {
							$modifiedImgPath = $newImgPath;
						}
					}
				}

				if ($stmt = $mysqli->prepare("UPDATE cat SET name = '$catName', description = '$catDescription', imgPath = '$modifiedImgPath' WHERE id=?")) {
					if (isset($cat_id)) {

						$stmt->bind_param("i", $cat_id);

						if ($stmt->execute()) {
							$catUpdated = true;
						}
					}
				}
			}

?>



		<!-- Page Content -->
		<div class="container">
			<!-- Page Heading -->
			<div class="row">
				<div class="small-box">
					<div class="col-lg-12">
						<hr>
							<h2 class="heading-text text-center">Edit cat</h2>
						<hr>
					</div>
				</div>
			</div> <!-- /.Page Heading -->

			<div class="row">
				<div class="box">
					<form method="post" enctype="multipart/form-data" name="modify-cat" id="modify-cat">
						<fieldset class="form-group">
							<label for="modified-cat-name">Cat name</label>
							<input type="text" name="modified-cat-name" id="modified-cat-name" class="form-control" value="<?php print $name;?>">
						</fieldset>
						<fieldset class="form-group">
							<label for="modified-cat-description">Cat Description</label>
							<textarea class="form-control" id="modified-cat-description" name="modified-cat-description" rows="10"><?php print $description;?></textarea>
						</fieldset>
						<fieldset class="form-group">
							<label for="cat-current-image">Current Image</label>
							<img src="<?php echo $imgPath;?>" id="cat-current-image" name="cat-current-image" style="width: 270px">
						</fieldset>
						<fieldset class="form-group">
							<label for="cat-new-image-upload">New Image</label>
							<input type="file" name="cat-new-image-upload" id="cat-image-upload">
						</fieldset>
						<input type="submit" id="modify-cat" name="modify-cat" value="Submit" class="btn btn-success">
					</form>
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/jquery.validate.min.js"></script>
		<script src="js/additional-methods.min.js"></script>
		<script src="js/modifyCat.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>

<?php
			if ($catUpdated) {
			?>
				<script>
					$('#modal-cat-updated').modal('toggle')
				</script>
			<?php
			}
?>
	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>