<?php
	session_start();
	include 'include/connect.php';
	
	$pageTitle = 'Email Subscription';
	$_SESSION['successful_submission'] = false;
	
	if(isset($_POST['btn-submit'])){
		$email = isset($_POST['email-subscription']) ? htmlentities($_POST['email-subscription'], ENT_QUOTES) : '';
		$stmt = "
			INSERT INTO 
				`emaillist`(
					`email`
				) 
			VALUES 
				(
					?
				)
		";
		if($sql = $mysqli->prepare($stmt)){				
			$sql->bind_param("s", $email);
			if($sql->execute()){
				require 'phpmailer/PHPMailerAutoload.php';
				$adminMail = new PHPMailer;
				$adminMail->setFrom('from@example.com', 'MyKittyCafe');
				$adminMail->addAddress($email);
				$adminMail->isHTML(true);
				$adminMail->Subject = 'Confirmation of MyKittyCafe Email Subscription';
				$adminMail->Body    = "
								<strong>
									Thank You for Subscribing to MyKittyCafe emailing list.
								</strong><br/>
								<strong>
									Description
								</strong>We are happy to share with you MyKittyCafe and Community Events. 
								Also, you will always be notify when we have sales and promotions.<br/>
								<br/>
							";
				$adminMail->AltBody = 'This is the body in plain text for non-HTML mail clients';
				if($adminMail->send()) {
					$_SESSION['successful_submission'] = true;
				} else {
					header("Location: index.php");
				}	
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
		include 'include/meta.php';
	?>

    <title>My Kitty Cafe</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/1-col-portfolio.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_success">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">You've been added.</h4>
      </div>
      <div class="modal-body">
        <p>Your email was successfully added to the mailing list.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
    <!-- Navigation -->
<?php
	include 'include/navbar.php';
?>

<div class="container">
	<div class="row">
		<div class="box">
			<div class="col-lg-12">
				<hr>
				<h2 class="intro-text text-center">My Kitty Cafe
				</h2>
				<hr>
				<hr class="visible-xs">
				<p>Located in the heart of Downtown Guelph. Offering delicious international dish, along with amazing smoothies and specialty cakes!  Don't forget to try our special coffee and tea!</p>
				<p>​13 Kittens from the opening day have been adopted and going home! Purrrrrfect! As of Jan 23rd, 40 kitties found forever home from My Kitty Café! New kittens are arriving daily to meet you!</p>
			</div>
		</div>
	</div>
</div>

<div class="container">
        <div class="row">
			<div class="box">
	            <div class="col-lg-12">
					<h2 class="intro-text text-center">Subsribe for the latest sales and promotions</h2>
	                 <div class="well">
	                     <form action="" method="POST" name='frm-email-subscription' id='frm-email-subscription'>
	                      <fieldset class="form-group">
	                        <label for="email-subscription">Email address</label>
	                        <input type="text" class="form-control" id="email-subscription" name="email-subscription" placeholder="Enter email">
	                        <small class="text-muted">We'll never share your email with anyone else.</small>
	                      </fieldset>
						  <span class="input-group-btn">
							<input class="btn btn-info"type="submit" name="btn-submit" value="Subscribe"/>
						  </span>
	                     </form>
	                 </div>
				</div>
			</div>
    </div>
</div>

<!-- jQuery -->

<!-- Bootstrap Core JavaScript -->
<?php
	include 'include/js.php';
?>
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script>
<script src="js/emailSubscription.js"></script>
<?php
	if($_SESSION['successful_submission']){
?>
	<script>$('#modal_success').modal('toggle')</script>
<?php
	}
?>

	
</body>

</html>
