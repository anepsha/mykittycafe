<?php
	session_start();
	include 'include/connect.php';
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			$pageTitle = 'Add New Cat';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		include 'include/meta.php';
	?>
		<title>
		<?php
			echo $pageTitle.' - My Kitty Cafe';
		?>
		</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
	<?php
		include 'include/navbar.php';
		include 'include/addNewCatModal.php';
		include 'include/addNewCatSuccessMes.php';
	?>

		<!-- Page Content -->
		<div class="container">

		<!-- Page Heading -->
			<div class="row">
				<div class="small-box">
					<div class="col-lg-12">
						<hr>
							<h2 class="heading-text text-center">Add new cat</h2>
						<hr>
					</div>
				</div>
			</div> <!-- /.Page Heading -->

			<div class="row">
				<div class="box">
					<div class="container">
						<div class="text-center">
							<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addNewCatModal" id="modify-cat-listing">Add New Cat</button>
							<button type="submit" class="btn btn-primary btn-lg" onclick="window.location.href='catsListing.php'">Modify Cat Listing</button>
						</div>
					</div>
				</div>
			</div>

		<script src="js/jquery.js"></script>
		<script src="js/jquery.validate.min.js"></script>
		<script src="js/additional-methods.min.js"></script>
		<script src="js/addNewCat.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>


		<?php
			$catAdded = false;
			$imgPath = '';

			function isNullOrEmptyString($input) {
				return (!isset($input) || trim($input)==='');
			}

			if (isset($_POST['add-new-cat']) && isset($_FILES['cat-image-upload'])) {


				$catName = isset($_POST['cat-name']) ? $mysqli->real_escape_string($_POST['cat-name']) : '';
				$catDescription = isset($_POST['cat-description']) ? $mysqli->real_escape_string($_POST['cat-description']) : '';

				/* Image upload*/
				$catImage = isset($_FILES['cat-image-upload']) ? $_FILES['cat-image-upload'] : '';
				$target_dir = "uploads/cats/";
				$imgPath = $target_dir . basename($catImage["name"]);

				if ($catImage["size"] <= 5000000) {
					move_uploaded_file($catImage["tmp_name"], $imgPath);
				}


				if (!isNullOrEmptyString($catName) && !isNullOrEmptyString($catDescription)) {
					$isAdopted = 1;
					$insertNewCatStmt = "INSERT INTO cat (name, description, isAdopted, imgPath) VALUES ('$catName', '$catDescription', '$isAdopted', '$imgPath')";

					if ($mysqli->query($insertNewCatStmt)) {
						$catAdded = true;
					}
				}
			}
			if ($catAdded) {
				?>
				<script>
					$('#modal-cat-added').modal('toggle')
				</script>
			<?php
			}
		?>
		<script>
			$("#cat-image-upload").on('change', function () {
			var imgPath = $(this)[0].value;
			var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();

		    if (extn == "png" || extn == "jpg" || extn == "jpeg") {
		        if (typeof (FileReader) != "undefined") {

		            var image_holder = $("#image-preview");
		            image_holder.empty();

		            var reader = new FileReader();
		            reader.onload = function (e) {
		                $("<img />", {
		                    "src": e.target.result,
		                        "class": "image-responsive",
		                        "style": "width: 270px"
		                }).appendTo(image_holder);

		            }
		            image_holder.show();
		            reader.readAsDataURL($(this)[0].files[0]);
		        } else {
		            alert("This browser does not support FileReader.");
		        }
		    } else {
		        alert("Pls select only images");
		    }
		});
		</script>

	</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>
