<?php
	session_start();
    include 'include/connect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    <?php
        include 'include/meta.php';
    ?>
        <title>Community Events</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/1-col-portfolio.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/kittyEventStyle.css">
        <link rel="stylesheet" type="text/css" href="css/styles.css">
    </head>
    <body>
    <?php
        include 'include/navbar.php';
    ?>
    <div class="container">
        <!-- Page Heading -->
        <div class="row">
            <div class="small-box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="heading-text text-center">Community Events</h2>
                    <hr>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <div class="row">
                        <a href='submitEvent.php?action=create' class='btn btn-default pull-right'>Create Event</a><br/>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="row">
                <?php
                    $stmt = "
                        SELECT
                            `title`,
                            `description`,
                            `dateStart`,
                            `dateEnd`,
                            `imagePath`,
                            `isApproved`,
                            `eventTypeId`,
                            `location`
                        FROM `event`
                        WHERE
                            isApproved = ?
                        AND
                            eventTypeId = ?
                    ";
                    $isApproved = 1;
                    $eventTypeId = 2;
                    $sql = $mysqli->prepare($stmt);
                    $sql->bind_param("ii", $isApproved, $eventTypeId);
                    $sql->execute();
                    $res = $sql->get_result();

                    $row_cn = $res->num_rows;

                    if ($row_cn == 0) {
                        echo '<h2 class="heading-text text-center">No scheduled events found</h2>';
                    }
                    while($row = $res->fetch_assoc()){
                ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <?php
                            echo $row['title'];
                        ?>
                        </div>
                        <div class="panel-body">
                            <div class="well">
                                <img src='<?php echo $row['imagePath']; ?>' class='img-responsive' style='margin: 0 auto'/>
                            </div>
                            <p class='vcenter' style='margin-left: 1em;'>
                                <strong>From</strong> <?php echo $row['dateStart']; ?> - <?php echo $row['dateEnd']; ?><br/>
                                <strong>Location</strong> <?php echo $row['location']; ?><br/>
                                <strong>Description</strong> <?php echo $row['description']; ?><br/>
                            </p>
                        </div>
                    </div>
                <?php
                    }
                ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        include 'include/js.php';
    ?>
    </body>
</html>
