<?php
    session_start();
	
	if($_SESSION['logged_in']){
		if($_SESSION['user_type'] == 1){
			include 'include/connect.php';
			
			if (isset($_GET['adopted'])) {
				$adopted = "";
				$changed = false;
				$catId = isset($_GET['adopted']) ? (int) $_GET['adopted'] : 0;
				$selectAdoptionStmt = "SELECT isAdopted FROM cat WHERE id=?";

				if ($stmt = $mysqli->prepare($selectAdoptionStmt)) {
					$stmt->bind_param("i", $catId);
					$stmt->execute();
					$stmt->bind_result($isCatAdopted);
					$stmt->fetch();
					$stmt->close();

					if ($isCatAdopted == 1) {
						$adopted = 0;
						$changeAdoptionStmt = "UPDATE cat SET isAdopted = '$adopted' WHERE id=?";
						if ($stmt = $mysqli->prepare($changeAdoptionStmt)) {
							$stmt->bind_param("i", $catId);
							if ($stmt->execute()) {
								$stmt->close();
								$changed = true;
							}
						}
					} else {
						$adopted = 1;
						$changeAdoptionStmt = "UPDATE cat SET isAdopted = '$adopted' WHERE id=?";
						if ($stmt = $mysqli->prepare($changeAdoptionStmt)) {
							$stmt->bind_param("i", $catId);
							if ($stmt->execute()) {
								$stmt->close();
								$changed = true;
							}
						}
					}
				}
				if ($changed == true) {
					header("Location: catsListing.php");
					exit();
				}
			}

			if (isset($_GET['deleted'])) {
				$deleted = false;
				$catId = isset($_GET['deleted']) ? (int) $_GET['deleted'] : 0;
				$deleteCatStmt = "DELETE FROM cat WHERE id=?";
				if ($stmt = $mysqli->prepare($deleteCatStmt)) {
					$stmt->bind_param("i", $catId);
					$stmt->execute();
					$stmt->close();
					$deleted = true;
				}
				if ($deleted == true) {
					header("Location: catsListing.php");
					exit();
				}
			}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		include 'include/meta.php';
	?>
    <title>Meows</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Custom CSS -->
    <link href="css/1-col-portfolio.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/modifyCatListingPhoto.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php
	include 'include/navbar.php';
	
    $result="";
    $result = $mysqli->query("SELECT id, name, description, isAdopted, imgPath FROM cat");
    $name="";
    $description="";
    $imgPath="";
    $id="";

	function description_echo($x, $length) {
		if(strlen($x)<=$length) {
            return $x;
		}
		else {
            $y=substr($x,0,$length) . '...';
            return $y;
        }
	};

    if ($result->num_rows === 0) {
        echo '<div class="container">';
			echo '<div class="box">';
				echo '<h2 class="heading-text text-center">No results found</h2>';
			echo '</div>';
		echo '</div>';
    } else{
            echo '<div class="container">';
                echo '<div class="box">';
        while ($row = $result->fetch_assoc()) {

            $id=(int) $row['id'];
            $name=trim($row['name']);
            $description=trim($row['description']);
            $imgPath=trim($row['imgPath']);
            $isAdopted = $row['isAdopted'];

			$new_description = description_echo($description, 100);

			echo '<div class="container">';
				echo '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">';
					echo '<h5 class="intro-text">';
						echo "Image";
					echo '</h5>';
					echo '<img class="img-responsive" src="'.$imgPath.'" alt="" style="margin-bottom : 10px">';
			echo '</div>';
                echo '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"">';
                    echo '<h5 class="intro-text">';
                    echo "Name";
                    echo '</h5>';
					echo "$name";
                echo '</div>';
				echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">';
                    echo '<h5 class="intro-text">';
					echo "Description:";
					echo '</h5>';
                    echo "$new_description";
				echo '</div>';
                echo '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">';
                    echo '<a class="btn btn-default" href="modifyCatListing.php?cat='.$id.'">';
                    echo 'Edit';
                    echo '</a>';
                    echo ' ';
				if ($isAdopted == 1) {
					echo '<a class="btn btn-success btn-cat-modify-style" href="catsListing.php?adopted='.$id.'" name="set-cat-adopted" id="set-cat-adopted" target="_self">';
                    echo 'Adopt';
                    echo '</a>';
                    echo ' ';
				} else {
					echo '<a class="btn btn-warning btn-cat-modify-style" href="catsListing.php?adopted='.$id.'" name="set-cat-adopted" id="set-cat-adopted">';
                    echo 'Adopted';
                    echo '</a>';
                    echo ' ';
				}
                    echo '<a class="btn btn-danger btn-cat-modify-style" href="catsListing.php?deleted='.$id.'" name="delete-cat">';
                    echo 'Delete';
                    echo '</a>';
                echo '</div>';

            echo '</div>';
            echo '<hr>';
        }
                echo '</div>';
            echo '</div>';

            $result->close();
    }
?>
</body>
</html>
<?php
		} else {
			unset($_SESSION['logged_in']);
			header('Location: login.php');
		}
	} else {
		header('Location: login.php');
	}
?>