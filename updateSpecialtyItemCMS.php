<!DOCTYPE html>
<html lang="en">
	<head>
	<?php
		session_start();
		include 'include/connect.php';
		include 'include/meta.php';
		include 'include/specialtyItemAddedSuccesMes.php';

		$modifiedImgPath="";
		$itemUpdated = false;

		if (isset($_GET['id'])) {

			$itemId = (int) $_GET['id'];
			if ($stmt = $mysqli->prepare("SELECT category, name, description, imgPath FROM specialtyitems WHERE id=?")) {
				$stmt->bind_param("i", $itemId);

				if ($stmt->execute()) {
					$stmt->bind_result($category, $name, $description, $imgPath);
					$stmt->fetch();
					$stmt->close();
				}

			}
		}

		if (isset($_POST['update-specialty-item'])) {

			$name = isset($_POST['item-name']) ? $mysqli->real_escape_string($_POST['item-name']) : '';
			$description = isset($_POST['item-description']) ? $mysqli->real_escape_string($_POST['item-description']) : '';
			$category = isset($_POST['item-category']) ? $_POST['item-category'] : '';
			$catImage = isset($_FILES['item-image']) ? $_FILES['item-image'] : '';
			$modifiedImgPath = $imgPath;

			if (!empty($catImage) && isset($catImage)) {

				$target_dir = "menuImage/";
				$newImgPath = $target_dir . basename($catImage['name']);

				if ($catImage["size"] <=500000) {
					if (move_uploaded_file($catImage["tmp_name"], $newImgPath)) {
						$modifiedImgPath = $newImgPath;
					}
				}
			}

			if ($stmt = $mysqli->prepare("UPDATE specialtyitems SET name = '$name', category = '$category', description = '$description', imgPath = '$modifiedImgPath' WHERE id=?")) {

				$stmt->bind_param("i", $itemId);

				if ($stmt->execute()) {
					$itemUpdated = true;
					$stmt->close();
				}
			}

		}
	?>
		<title>My Kitty Cafe</title>

		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="css/1-col-portfolio.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
		<br/>
			<div class="row">
				<div class="box">
					<div class="col-lg-12">
						<!-- insert content here -->
						<h1 class='text-center'>
							Update Specialty Item
						</h1><br/>
						<form class='form-horizontal' method="post" name="frm-update-speciality-item" id="frm-update-speciality-item" enctype="multipart/form-data">
							<div class='form-group'>
								<label class='control-label col-xs-6'>
									Category
								</label>
								<div class='col-xs-6'>
									<select class='form-control' name="item-category">
							<?php 
								if ($category == 'Drinks') {
									?>
									<option value="Drinks" selected = "selected">Drinks</option>
									<option value="Snacks">Snacks</option>
									<option value="Food">Food</option>
									<?php
								} else if ($category == 'Snacks') {
									?>
									<option value="Drinks">Drinks</option>
									<option value="Snacks" selected = "selected">Snacks</option>
									<option value="Food">Food</option>
									<?php
								} else {
									?>
									<option value="Drinks">Drinks</option>
									<option value="Food" selected = "selected">Food</option>
									<option value="Snacks">Snacks</option>
									<?php
								}
							?>
										

									</select>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-xs-6'>
									Name
								</label>
								<div class='col-xs-6'>
									<input type='text' class='form-control' name="item-name" id="item-name" value="<?php echo $name?>" />
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-xs-6'>
									Description
								</label>
								<div class='col-xs-6'>
									<textarea class='form-control' rows='3' name="item-description" id="item-description"><?php echo $description ?></textarea>
								</div>
							</div>
							<div class='form-group'>
								<label for='' class='control-label col-xs-6'>
									Image
								</label>
								<div class='col-xs-6'>
									<input type="file" id="item-image" name="item-image">
									<p class="help-block">
										.png or .jpg only.
									</p>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-xs-6'>
								</label>
								<div class='col-xs-6'>
									<input type='submit' class='btn btn-success' value='Submit' name="update-specialty-item" id="update-specialty-item" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php
		include 'include/js.php';

		if ($itemUpdated) {
			?>
			<script>
				$('#modal-specialty-added').modal('toggle')
			</script>
			<?php
		}
	?>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/updateSpecialityItem.js"></script>
	</body>
</html>
